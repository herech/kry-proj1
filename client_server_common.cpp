/*
 * Autor: Jan Herec (xherec00)
 * Datum: 16. března 2017
 * Soubor: client_server_common.cpp
 * Komentar: Zdrojový soubor představující implementaci společných funkcí, které využívají klient i server
 * Kodovani: UTF-8
 */ 

#include <stdlib.h>
#include <stdio.h>
#include <cstdlib>
#include <unistd.h>
#include <string>
#include <vector>
#include <iostream>
#include <fstream>
#include <sys/types.h>
#include <new>   
#include <deque>
#include <math.h> 
#include <limits>
#include <string.h>

#include <sys/stat.h>
#include <fcntl.h>
#include <errno.h>

#include <openssl/conf.h>
#include <openssl/evp.h>
#include <openssl/err.h>
#include <openssl/dh.h>
#include <openssl/bn.h>

#include "client_server_common.hpp"


// ########################## PROTOTYPY FUNKCÍ #################################

/**
 * Funkce je vygenerivába pomocí: openssl dhparam -C 2048
 * Vrací alkovoanou strukturu DH naplněnou parametry
 * @return struktura DH naplněná parametry
 */
DH *get_dh2048();

/**
 * Funkce zapíše do pojemnované roury počet bajtů, které představují velikost následně zaslané zprávy, aby druhá protistrana věděla kdy má se čtením skončit, tak aby přečetla celou zprávu
 * @param numberOfBytes počet bajtů následně zaslné zprávy
 * @param pipeDescriptor dektiptor otevřené roury pro zápis
 * @return 0, pokud se operace podařila, jinak -1
 */
int sendNumBytesToWrite(int numberOfBytes, int pipeDescriptor);

/**
 * Funkce počle přes pojmenovanou rouru veřejný klíč z DH protistraně
 * @param publicKeyInHex veřejný klíč, který chcem poslat protistraně
 * @param pipeDescriptor deskriptor otevřené roury pro zápis
 * @return 0, pokud se operace podařila, jinak -1
 */
int sendPublicKeyToPeer(char * publicKeyInHex, int pipeDescriptor);

/**
 * Funkce přečte z pojemnované roury počet bajtů, které představují velikost následně zaslané zprávy, víme tak, kolik bajtů přečíst, abychom přečetli celou zprávu
 * @param pipeDescriptor  deskriptor otevřené roury pro čtení
 * @return počet bajtů, které je třeba načíst, abchom načetli celou zaslanou zprávu nebo v případě chyb -1
 */
int getNumBytesToRead(int pipeDescriptor);

/**
 * Funkce z pojmenované roury přečte veřejný klíč zaslaný protistranou
 * @param pipeDescriptor deskriptor otevřené roury pro čtení
 * @param publicKey výstupní parametr, přes který funkce předá načtený veřejný klíč z roury
 * @return 0, pokud se operace podařila, jinak -1
 */
int receivePublicKeyFromPeer(int pipeDescriptor, BIGNUM **publicKey);

/**
 * Funkce která vypočítá sdílené tajemství u DH algoritmu
 * @param secret výstupní parametr představující vypočítané sdílené tajemství
 * @param publicKey veřejný klíč protistrany
 * @param publicPrivateKeyPair pár soukromého a veřejného klíče naší strany
 * @return délka (v bajtech) sdíleného tajemství nebo -1 v případě chyby
 */
int computeSharedSecret(unsigned char **secret, BIGNUM *publicKey, DH *publicPrivateKeyPair);

/**
 * Funkce na úklid v souvislosti OPenSLL knihovnou
 */
void openSSLcleanUp();

/**
 * Funkce na inicializaci v souvislosti OPenSLL knihovnou
 */
void openSSLInit();

/**
 * Funkce vygeneruje v rámci DH algoritmu pár soukromého a veřejného klíče, klíče mají velikost 2048b
 * @param publicPrivateKeyPair výstupní parametr představující  strukturu obsahující vygenerovaný pár soukromého a veřejného klíče 
 * @return 0, pokud se operace podařila, jinak -1
 */
int generatePublicPrivateKeyPair(DH **publicPrivateKeyPair);

/**
 * Získáme redukovaný 256bit klíč z půsovního 2048bit klíče a to tak, že vezmeme každý sudý bajt od začátku tohoto klíče, dokud nedostaneme počet bajtů 32 = počet bitů 256
 * @param key sdílené tajmeství získané z DH
 * @return tajný 256bit klíč pro symetrické  šifrování 
 */
unsigned char * reduce2048BitKeyTo256bits(unsigned char *key);

/**
 * Získáme inicializační vektor z půsovního 2048bit klíče a to tak, že vezmeme reverzovaně jeho posledních 128 bitů
 * @param key sdílené tajmeství získané z DH
 * @return 128 bit inicializační vektor pro symetrické šifrování
 */
unsigned char * get128BitIVFrom2048BitKey(unsigned char *key);

/**
 * Funkce šifruje otevřený text pomocí AES CBC a 256bit klíče. Je převzata ze stránek https://wiki.openssl.org/index.php/EVP_Symmetric_Encryption_and_Decryption
 * jakožto best practise a pro potřeby projektu mírně upravena
 * @param plaintext otevřený text, který bude šifrován
 * @param plaintext_len délka otevřeho textu (v bajtech)
 * @param key 256bit tajný klíč, kterým se bude šifrovat
 * @param iv 128bit Inicializační vektor
 * @param ciphertext výstupní parametr představující zašifrovaný text
 * @return vrací délku zašifrovaného textu
 */
int encrypt(unsigned char *plaintext, int plaintext_len, unsigned char *key, unsigned char *iv, unsigned char *ciphertext);

/**
 * Funkce dešifruje šifrovaný text. Je převzata ze stránek https://wiki.openssl.org/index.php/EVP_Symmetric_Encryption_and_Decryption
 * jakožto best practise a pro potřeby projektu mírně upravena
 * @param ciphertext šifrovaný text, který bude dešifrován
 * @param ciphertext_len délka šifrovaného textu (v bajtech)
 * @param key 256bit tajný klíč, kterým se bude dešifrovat
 * @param iv 128bit Inicializační vektor
 * @param plaintext  výstupní parametr představující defišofrovaný text
 * @return vrací délku dešifrovaného textu
 */
int decrypt(unsigned char *ciphertext, int ciphertext_len, unsigned char *key, unsigned char *iv, unsigned char *plaintext);


/**
 * Funkce zašle šifrovanou zprávu protistraně přes pojemnovanou rouru 
 * @param cipherMessage šifrovaná zpráva
 * @param cipherMessageLength délka šifrované zprávy
 * @param pipeDescriptor deskriptor pojemnované roury otevřené pro zápis
 * @return 0, pokud vše proběhlo v pořádku, jinak -1
 */
int sendCipherMessageToPeer(unsigned char * cipherMessage, int cipherMessageLength, int pipeDescriptor);

/**
 * Funkce získá přes pojmenovanou rouru šifrovanou zprávu od protistrany
 * @param pipeDescriptor deskriptor pojemnované roury otevřené pro čtení
 * @param cipherMessage výstupní parametr představující získanou šifrovanou zprávu
 * @return -1 pokud chyba jinka počet načtených bytů (velikost zašifroané zprávy)
 */
int receiveCipherMessageFromPeer(int pipeDescriptor, unsigned char **cipherMessage);

/**
 * Funkce vytvoří hash zprávy pomocí SHA-256. Je převzata ze stránek https://wiki.openssl.org/index.php/EVP_Message_Digests
 * jakožto best practise a pro potřeby projektu mírně upravena
 * @param message zpráva, ze které bude vytvářen hash
 * @param message_len délka zprávy, ze které bude vytvářen hash
 * @param digest výstupní parametr představuje hash zprávy
 * @param digest_len délka hashe zprávy
 * @return 0, pokud nastala chyba tak -1
 */
int digestMessageWithSHA256(const unsigned char *messageToDigest, size_t messageToDigestLenght, unsigned char **digestOfMessage, unsigned int *digestOfMessageLength);

// ######################### DEFINICE FUNKCÍ ###################################

/**
 * Funkce zapíše do pojemnované roury počet bajtů, které představují velikost následně zaslané zprávy, aby druhá protistrana věděla kdy má se čtením skončit, tak aby přečetla celou zprávu
 * @param numberOfBytes počet bajtů následně zaslné zprávy
 * @param pipeDescriptor dektiptor otevřené roury pro zápis
 * @return 0, pokud se operace podařila, jinak -1
 */
int sendNumBytesToWrite(int numberOfBytes, int pipeDescriptor) {
    
    char strToWrite[50]; // řetězec obsahující počet bajtů k zápisu

    sprintf(strToWrite, "%d.", numberOfBytes); // převedeme int na řetězec + .

    int allWritedBytes = 0;
    int bytesToWrite = strlen(strToWrite);
    // pokoušíme se zapsat strToWrite (počet bajtů), write nám ovšem negarantuje, že obsah strToWrite zapíše celý najednou, musíme to tak zkoušet v cyklu
    while(1) {
        int currentlyWritedBytes = 0;
        currentlyWritedBytes = write(pipeDescriptor, strToWrite + allWritedBytes, bytesToWrite - allWritedBytes);
        // chyba
        if (currentlyWritedBytes < 0) {
            return -1;
        }
        
        allWritedBytes += currentlyWritedBytes;
        // podařilo se nám zapsat počet bajtů, můžeme cyklus zápisu ukončit
        if (bytesToWrite == allWritedBytes) break;
    }
    
    return 0;
}

/**
 * Funkce počle přes pojmenovanou rouru veřejný klíč z DH protistraně
 * @param publicKeyInHex veřejný klíč, který chcem poslat protistraně
 * @param pipeDescriptor deskriptor otevřené roury pro zápis
 * @return 0, pokud se operace podařila, jinak -1
 */
int sendPublicKeyToPeer(char * publicKeyInHex, int pipeDescriptor) {
    
    // nejdřív zapíšeme do roury počet bajtů klíče
    if (sendNumBytesToWrite(strlen(publicKeyInHex), pipeDescriptor) == -1) {
        return -1;
    }
    
    int allWritedBytes = 0;
    int bytesToWrite = strlen(publicKeyInHex);
    // pokoušíme se zapsat publicKeyInHex, write nám ovšem negarantuje, že obsah publicKeyInHex zapíše celý najednou, musíme to tak zkoušet v cyklu
    while(1) {
        int currentlyWritedBytes = 0;
        currentlyWritedBytes = write(pipeDescriptor, publicKeyInHex + allWritedBytes, bytesToWrite - allWritedBytes);
        // chyba
        if (currentlyWritedBytes < 0) {
            return -1;
        }
        
        allWritedBytes += currentlyWritedBytes;
        // podařilo se nám zapsat poslat celý klíč, můžeme cyklus zápisu ukončit
        if (bytesToWrite == allWritedBytes) break;
    }
    
    return 0;

}

/**
 * Funkce přečte z pojemnované roury počet bajtů, které představují velikost následně zaslané zprávy, víme tak, kolik bajtů přečíst, abychom přečetli celou zprávu
 * @param pipeDescriptor  deskriptor otevřené roury pro čtení
 * @return počet bajtů, které je třeba načíst, abchom načetli celou zaslanou zprávu nebo v případě chyb -1
 */
int getNumBytesToRead(int pipeDescriptor) {
    
    char strToRead[50]; // řetezec do kterého budeme načítat počet bajtů k přečtení

    int allReadedBytes = 0;
    // po jednom bajtu načítáme z pojmenované roury
    while(1) {
        int currentlyReadedBytes = 0;
        currentlyReadedBytes = read(pipeDescriptor, strToRead + allReadedBytes, 1);
        if (currentlyReadedBytes <= 0) {
            return -1;
        }
                  
        // pokud jsme načetli již počet bytů ke čtení
        if (strToRead[allReadedBytes] == '.') break;
        
        allReadedBytes += currentlyReadedBytes;
    }
    
    strToRead[allReadedBytes] = '\0';
    
    int numBytesToRead = atoi(strToRead);
    // pokud se konverze řetězce na číslo nezdařila
    if (numBytesToRead == 0) return -1;
    
    return numBytesToRead;
}

/**
 * Funkce z pojmenované roury přečte veřejný klíč zaslaný protistranou
 * @param pipeDescriptor deskriptor otevřené roury pro čtení
 * @param publicKey výstupní parametr, přes který funkce předá načtený veřejný klíč z roury
 * @return 0, pokud se operace podařila, jinak -1
 */
int receivePublicKeyFromPeer(int pipeDescriptor, BIGNUM **publicKey) {

    int bytesToRead = 0;
    
    // přečteme z pojemnované roury počet bajtů klíče, který nám proristrana zaslala
    if ((bytesToRead = getNumBytesToRead(pipeDescriptor)) == -1) {
        return -1;
    }
    
    // řetezec, který bude obsahovat načtený veřejný klíč
    char *strToRead = new (std::nothrow) char[bytesToRead + 1]; 
    if (strToRead == NULL) return -1;

    int allReadedBytes = 0;
    // pokoušíme se přečíst veřejný klíč zaslaný protistranou, read nám ovšem negarantuje, že přečtě z roury počet bajtů: bytesToRead najednou, musíme to tak zkoušet v cyklu
    while(1) {
        int currentlyReadedBytes = 0;
        currentlyReadedBytes = read(pipeDescriptor, strToRead + allReadedBytes, bytesToRead - allReadedBytes);

        if (currentlyReadedBytes <= 0) {
            return -1;
        }
        
        allReadedBytes += currentlyReadedBytes;
        // načetli jsme celý klíč z roury, můžeme ukončit cyklus čtení
        if (bytesToRead == allReadedBytes) break;
    }
    
    strToRead[allReadedBytes] = '\0';
    
    // načtený klíč převedeme z řetezcobé hexadecimální podoby do podoby struktury BIGNUMBER
    if(0 == (BN_hex2bn(publicKey, strToRead))) return -1;
    //BN_print_fp(stdout, publicKey);
    //fflush(stdout);
    
    return 0;

}

/**
 * Funkce která vypočítá sdílené tajemství u DH algoritmu
 * @param secret výstupní parametr představující vypočítané sdílené tajemství
 * @param publicKey veřejný klíč protistrany
 * @param publicPrivateKeyPair pár soukromého a veřejného klíče naší strany
 * @return délka (v bajtech) sdíleného tajemství nebo -1 v případě chyby
 */
int computeSharedSecret(unsigned char **secret, BIGNUM *publicKey, DH *publicPrivateKeyPair) {

    // alkoace místa pro sdílené tajemství
    if(NULL == (*secret = (unsigned char*) OPENSSL_malloc(sizeof(unsigned char) * (DH_size(publicPrivateKeyPair))))) return -1;

    int secretSize;

    // výpočet sdíleného tajemství na základě páru soukromého a veřejného klíče naší strany a veřejného klíče protistrany
    if(0 > (secretSize = DH_compute_key(*secret, publicKey, publicPrivateKeyPair))) return -1;

    return secretSize;
}

/**
 * Funkce na úklid v souvislosti OPenSLL knihovnou
 */
void openSSLcleanUp() {

    EVP_cleanup();

    CRYPTO_cleanup_all_ex_data();

    ERR_free_strings();
}

/**
 * Funkce na inicializaci v souvislosti OPenSLL knihovnou
 */
void openSSLInit() {
    
    ERR_load_crypto_strings();

    OpenSSL_add_all_algorithms();

    OPENSSL_config(NULL);
}

/**
 * Funkce vygeneruje v rámci DH algoritmu pár soukromého a veřejného klíče, klíče mají velikost 2048b
 * @param publicPrivateKeyPair výstupní parametr představující  strukturu obsahující vygenerovaný pár soukromého a veřejného klíče 
 * @return 0, pokud se operace podařila, jinak -1
 */
int generatePublicPrivateKeyPair(DH **publicPrivateKeyPair) {
    // nejdřív vytvoříme strukturu DH na základě předgenerovaných parametrů
    DH *tmpPublicPrivateKeyPair = get_dh2048();
    
    /* Vygenerujeme pár veřejného a soukromého klíče o velikost 2048 bitů na základě předgenerovaných parametrů */
    if(1 != DH_generate_key(tmpPublicPrivateKeyPair)) return -1;
    
    *publicPrivateKeyPair = tmpPublicPrivateKeyPair;
    
    return 0;
}

/**
 * Získáme redukovaný 256bit klíč z půsovního 2048bit klíče a to tak, že vezmeme každý sudý bajt od začátku tohoto klíče, dokud nedostaneme počet bajtů 32 = počet bitů 256
 * @param key sdílené tajmeství získané z DH
 * @return tajný 256bit klíč pro symetrické  šifrování 
 */
unsigned char * reduce2048BitKeyTo256bits(unsigned char *key) {

    // redukovaný klíč
    unsigned char *reducedKey = new (std::nothrow) unsigned char[32]; 
    if (reducedKey == NULL) return NULL;

    // redukovaný 256 klíč získáme z původního 2048 bit DH tajného klíče, tak že vezmeme jeho každý sudý bajt, dokud jich nezískáme 32
    for (int i = 0; i < 32; i++) {
        reducedKey[i] = (unsigned char) key[i*2];//key[i];
    }
    
    return reducedKey;

}

/**
 * Získáme inicializační vektor z půsovního 2048bit klíče a to tak, že vezmeme reverzovaně jeho posledních 128 bitů
 * @param key sdílené tajmeství získané z DH
 * @return 128 bit inicializační vektor pro symetrické šifrování
 */
unsigned char * get128BitIVFrom2048BitKey(unsigned char *key) {
    unsigned char *iv = new (std::nothrow) unsigned char[16]; 
    if (iv == NULL) return NULL;
    
    // 128bit IV získáme z původního 2048 bit DH tajného klíče, tak že vezmeme reverzovaně jeho posledních 128 bitů
    for (int i = 0; i < 16; i++) {
        iv[i] = key[256 - i];
    }
    
    return iv;

}

/**
 * Funkce šifruje otevřený text pomocí AES CBC a 256bit klíče. Je převzata ze stránek https://wiki.openssl.org/index.php/EVP_Symmetric_Encryption_and_Decryption
 * jakožto best practise a pro potřeby projektu mírně upravena
 * @param plaintext otevřený text, který bude šifrován
 * @param plaintext_len délka otevřeho textu (v bajtech)
 * @param key 256bit tajný klíč, kterým se bude šifrovat
 * @param iv 128bit Inicializační vektor
 * @param ciphertext výstupní parametr představující zašifrovaný text
 * @return vrací délku zašifrovaného textu
 */
int encrypt(unsigned char *plaintext, int plaintext_len, unsigned char *key, unsigned char *iv, unsigned char *ciphertext) {
    EVP_CIPHER_CTX *ctx;

    int len;

    int ciphertext_len;

    /* Create and initialise the context */
    if(!(ctx = EVP_CIPHER_CTX_new())) return -1;

    /* Initialise the encryption operation. IMPORTANT - ensure you use a key
     * and IV size appropriate for your cipher
     * In this example we are using 256 bit AES (i.e. a 256 bit key). The
     * IV size for *most* modes is the same as the block size. For AES this
     * is 128 bits */
    if(1 != EVP_EncryptInit_ex(ctx, EVP_aes_256_cbc(), NULL, key, iv)) return -1;

    /* Provide the message to be encrypted, and obtain the encrypted output.
     * EVP_EncryptUpdate can be called multiple times if necessary
     */
    if(1 != EVP_EncryptUpdate(ctx, ciphertext, &len, plaintext, plaintext_len)) return -1;
    
    ciphertext_len = len;

    /* Finalise the encryption. Further ciphertext bytes may be written at
     * this stage.
     */
    if(1 != EVP_EncryptFinal_ex(ctx, ciphertext + len, &len)) return -1;
    ciphertext_len += len;

    /* Clean up */
    EVP_CIPHER_CTX_free(ctx);

    return ciphertext_len;
}

/**
 * Funkce dešifruje šifrovaný text. Je převzata ze stránek https://wiki.openssl.org/index.php/EVP_Symmetric_Encryption_and_Decryption
 * jakožto best practise a pro potřeby projektu mírně upravena
 * @param ciphertext šifrovaný text, který bude dešifrován
 * @param ciphertext_len délka šifrovaného textu (v bajtech)
 * @param key 256bit tajný klíč, kterým se bude dešifrovat
 * @param iv 128bit Inicializační vektor
 * @param plaintext  výstupní parametr představující defišofrovaný text
 * @return vrací délku dešifrovaného textu
 */
int decrypt(unsigned char *ciphertext, int ciphertext_len, unsigned char *key, unsigned char *iv, unsigned char *plaintext) {
    EVP_CIPHER_CTX *ctx;

    int len;

    int plaintext_len;

    /* Create and initialise the context */
    if(!(ctx = EVP_CIPHER_CTX_new())) return -1;

    /* Initialise the decryption operation. IMPORTANT - ensure you use a key
     * and IV size appropriate for your cipher
     * In this example we are using 256 bit AES (i.e. a 256 bit key). The
     * IV size for *most* modes is the same as the block size. For AES this
     * is 128 bits */
    if(1 != EVP_DecryptInit_ex(ctx, EVP_aes_256_cbc(), NULL, key, iv)) return -1;

    /* Provide the message to be decrypted, and obtain the plaintext output.
     * EVP_DecryptUpdate can be called multiple times if necessary
     */
    if(1 != EVP_DecryptUpdate(ctx, plaintext, &len, ciphertext, ciphertext_len)) return -1;
    plaintext_len = len;

    /* Finalise the decryption. Further plaintext bytes may be written at
     * this stage.
     */
    if(1 != EVP_DecryptFinal_ex(ctx, plaintext + len, &len)) return -1;
    plaintext_len += len;

    /* Clean up */
    EVP_CIPHER_CTX_free(ctx);

    return plaintext_len;
}

/**
 * Funkce zašle šifrovanou zprávu protistraně přes pojemnovanou rouru 
 * @param cipherMessage šifrovaná zpráva
 * @param cipherMessageLength délka šifrované zprávy
 * @param pipeDescriptor deskriptor pojemnované roury otevřené pro zápis
 * @return 0, pokud vše proběhlo v pořádku, jinak -1
 */
int sendCipherMessageToPeer(unsigned char * cipherMessage, int cipherMessageLength, int pipeDescriptor) {
    
    // nejdřív pošleme protistraně počet bajtů, které budeme posílat (délku šifrovaného textu)
    if (sendNumBytesToWrite(cipherMessageLength, pipeDescriptor) == -1) {
        return -1;
    }
    
    int allWritedBytes = 0;
    int bytesToWrite = cipherMessageLength;

    // pokoušíme se poslat cipherMessage, write nám ovšem negarantuje, že obsah cipherMessage zapíše celý najednou, musíme to tak zkoušet v cyklu
    while(1) {
        int currentlyWritedBytes = 0;
        currentlyWritedBytes = write(pipeDescriptor, cipherMessage + allWritedBytes, bytesToWrite - allWritedBytes);
        if (currentlyWritedBytes < 0) {
            return -1;
        }
        
        allWritedBytes += currentlyWritedBytes;
        // poslána celá zašifrovaná zpráva, ukončíme zápisový cyklus
        if (bytesToWrite == allWritedBytes) break;
    }
    
    return 0;

}

/**
 * Funkce získá přes pojmenovanou rouru šifrovanou zprávu od protistrany
 * @param pipeDescriptor deskriptor pojemnované roury otevřené pro čtení
 * @param cipherMessage výstupní parametr představující získanou šifrovanou zprávu
 * @return -1 pokud chyba jinka počet načtených bytů (velikost zašifroané zprávy)
 */
int receiveCipherMessageFromPeer(int pipeDescriptor, unsigned char **cipherMessage) {

    int bytesToRead = 0;
    
    // nejdřív zistíme počet bajtů, které budeme číst (délku šifrovaného textu)
    if ((bytesToRead = getNumBytesToRead(pipeDescriptor)) == -1) {
        return -1;
    }
    
    // načtený šifrovaný text
    unsigned char *strToRead = new (std::nothrow) unsigned char[bytesToRead]; 
    if (strToRead == NULL) return -1;

    int allReadedBytes = 0;
    // pokoušíme se načíst šifrovanou zprávu, read nám ovšem negarantuje, že tuto zprávu přečte celou najednou, musíme to tak zkoušet v cyklu
    while(1) {
        int currentlyReadedBytes = 0;//bytesToRead - allReadedBytes
        currentlyReadedBytes = read(pipeDescriptor, strToRead + allReadedBytes, bytesToRead - allReadedBytes);

        if (currentlyReadedBytes <= 0) {
            return -1;
        }
        
        allReadedBytes += currentlyReadedBytes;
        // získána celá zašifrovaná zpráva, ukončíme čtecí cyklus
        if (bytesToRead == allReadedBytes) break;
    }
    
    // načtenou zprávu předáme do výstupního parametru 
    *cipherMessage = strToRead;
    
    return bytesToRead;

}

/**
 * Funkce vytvoří hash zprávy pomocí SHA-256. Je převzata ze stránek https://wiki.openssl.org/index.php/EVP_Message_Digests
 * jakožto best practise a pro potřeby projektu mírně upravena
 * @param message zpráva, ze které bude vytvářen hash
 * @param message_len délka zprávy, ze které bude vytvářen hash
 * @param digest výstupní parametr představuje hash zprávy
 * @param digest_len délka hashe zprávy
 * @return 0, pokud nastala chyba tak -1
 */
int digestMessageWithSHA256(const unsigned char *messageToDigest, size_t messageToDigestLenght, unsigned char **digestOfMessage, unsigned int *digestOfMessageLength) {
    EVP_MD_CTX *mdctx;

    if ((mdctx = EVP_MD_CTX_create()) == NULL) return -1;

    if (1 != EVP_DigestInit_ex(mdctx, EVP_sha256(), NULL)) return -1;

    if (1 != EVP_DigestUpdate(mdctx, messageToDigest, messageToDigestLenght)) return -1;

    if ((*digestOfMessage = (unsigned char *) OPENSSL_malloc(EVP_MD_size(EVP_sha256()))) == NULL) return -1;

    if (1 != EVP_DigestFinal_ex(mdctx, *digestOfMessage, digestOfMessageLength)) return -1;

    EVP_MD_CTX_destroy(mdctx);
    
    return 0;
}

/**
 * Funkce je vygenerivába pomocí: openssl dhparam -C 2048
 * Vrací alkovoanou strukturu DH naplněnou parametry
 * @return struktura DH naplněná parametry
 */
DH *get_dh2048() {
    static unsigned char dh2048_p[] = {
        0xA8, 0x5E, 0xEC, 0x53, 0x1F, 0x76, 0xD5, 0xEE, 0x57, 0xA6, 0xFB, 0x65,
        0x2A, 0x03, 0xB1, 0x32, 0x73, 0x6F, 0xC1, 0x93, 0x48, 0xBD, 0xD9, 0x1D,
        0xE0, 0x43, 0x2E, 0x22, 0x70, 0xBD, 0x90, 0x46, 0x35, 0xCE, 0x8A, 0x17,
        0xAB, 0xDC, 0xF1, 0x2D, 0x1B, 0x3E, 0xDD, 0xFC, 0x9B, 0xDA, 0xFF, 0x20,
        0x57, 0x9C, 0x59, 0xD6, 0xB2, 0x9C, 0x0D, 0x62, 0x55, 0x7D, 0xC3, 0xE5,
        0x8A, 0xF0, 0x03, 0xFC, 0xC8, 0x64, 0x3B, 0x46, 0x99, 0x55, 0x65, 0x3C,
        0x5A, 0x5C, 0x3B, 0x5F, 0xF2, 0xE0, 0xC1, 0x6F, 0xB7, 0x30, 0x4F, 0x4C,
        0xA1, 0x49, 0x18, 0x46, 0xF3, 0x2F, 0xF0, 0x49, 0xE4, 0xD1, 0x5E, 0xBF,
        0x65, 0xF0, 0x9D, 0xEA, 0x09, 0x55, 0x56, 0x62, 0xEA, 0x86, 0x84, 0x10,
        0xB8, 0xFC, 0x9C, 0xCE, 0x05, 0x19, 0x4D, 0xC4, 0x64, 0x1F, 0xCF, 0x23,
        0x1E, 0x93, 0x00, 0x35, 0x6D, 0x88, 0xBA, 0x0C, 0xAC, 0xE4, 0x25, 0xBF,
        0x47, 0x52, 0x3F, 0x05, 0x31, 0x68, 0x1C, 0xD8, 0x9C, 0x79, 0x92, 0x51,
        0x1F, 0xA4, 0x59, 0x29, 0x54, 0x11, 0x7B, 0xC1, 0x68, 0xEA, 0xA1, 0x09,
        0x23, 0xEE, 0xF3, 0xEA, 0x0D, 0x0F, 0x2F, 0x2B, 0xB9, 0x94, 0xEB, 0x24,
        0x87, 0xCF, 0xFA, 0x92, 0x74, 0xC1, 0xAD, 0x41, 0x06, 0xB3, 0xD0, 0x9B,
        0xE2, 0x19, 0x8F, 0xE2, 0x7C, 0x1A, 0xB3, 0x3F, 0x2B, 0x40, 0x5F, 0x5A,
        0xF3, 0x2E, 0x72, 0x71, 0x91, 0xBF, 0x55, 0x27, 0x42, 0xB6, 0x51, 0xAF,
        0x9D, 0x82, 0x07, 0x97, 0x07, 0x52, 0xB6, 0x40, 0xED, 0x69, 0x0E, 0x8C,
        0xA6, 0x42, 0x72, 0x93, 0xBF, 0xB3, 0x0F, 0x22, 0x89, 0x7D, 0x66, 0xC7,
        0x41, 0xC1, 0x6D, 0x13, 0x1F, 0xEA, 0x55, 0x92, 0xE6, 0xF8, 0x85, 0xAE,
        0x3F, 0x67, 0x6B, 0xB4, 0xA2, 0x5A, 0x45, 0x13, 0xC4, 0x53, 0xBF, 0x0F,
        0x10, 0x52, 0xE5, 0x4B,
    };
    static unsigned char dh2048_g[] = {
        0x02,
    };
    
    DH *dh;

    if ((dh = DH_new()) == NULL) return (NULL);
    dh->p = BN_bin2bn(dh2048_p, sizeof (dh2048_p), NULL);
    dh->g = BN_bin2bn(dh2048_g, sizeof (dh2048_g), NULL);
    if ((dh->p == NULL) || (dh->g == NULL)) {
        DH_free(dh);
        return (NULL);
    }
    return (dh);
}
/*
 -----BEGIN DH PARAMETERS-----
MIIBCAKCAQEAqF7sUx921e5XpvtlKgOxMnNvwZNIvdkd4EMuInC9kEY1zooXq9zx
LRs+3fyb2v8gV5xZ1rKcDWJVfcPlivAD/MhkO0aZVWU8Wlw7X/LgwW+3ME9MoUkY
RvMv8Enk0V6/ZfCd6glVVmLqhoQQuPyczgUZTcRkH88jHpMANW2Iugys5CW/R1I/
BTFoHNiceZJRH6RZKVQRe8Fo6qEJI+7z6g0PLyu5lOskh8/6knTBrUEGs9Cb4hmP
4nwasz8rQF9a8y5ycZG/VSdCtlGvnYIHlwdStkDtaQ6MpkJyk7+zDyKJfWbHQcFt
Ex/qVZLm+IWuP2drtKJaRRPEU78PEFLlSwIBAg==
-----END DH PARAMETERS-----
*/