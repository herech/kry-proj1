Tunnel for secure messaging

This project is aimed at creating a tunnel for secure messaging between two parties - the client and the server. To secure communication will apply the provisions of keys using Diffie-Hellman, identification by Feige-Fiat-Shamir scheme, symmetric encryption, AES and SHA256 hash function.