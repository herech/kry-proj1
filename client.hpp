/*
 * Autor: Jan Herec (xherec00)
 * Datum: 16. března 2017
 * Soubor: client.hpp
 * Komentar: Hlavičkový soubor představující implementaci klienta, který bude komunikovat se serverem pomocí zabezpečeného tunelu
 * Kodovani: UTF-8
 */ 

#ifndef CLIENT_HPP
#define CLIENT_HPP

// jméno pojmenované roury určené ke komunikaci ve směru: server -> klient
#define NAMED_PIPE_FROM_SERVER_TO_CLIENT "/tmp/xherec00_KRY_NAMED_PIPE_FROM_SERVER_TO_CLIENT"

/**
 * Funkce představuje životní cyklus klienta (autentizuje se serveru a komunikuje s ním pomocí zabezpečeného tunelu, po zaslání všech zpráv serveru končí)
 */
void startClient();

#endif /* CLIENT_HPP */

