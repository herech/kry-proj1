all: main
main:
		g++ -pedantic -std=c++11 client.cpp server.cpp main.cpp client_server_common.cpp -o proj1 -lgmp -lssl -lcrypto
debug:
		g++ -pedantic -ggdb3 -std=c++11 client.cpp server.cpp main.cpp client_server_common.cpp -o proj1 -lgmp -lssl -lcrypto


