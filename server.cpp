/*
 * Autor: Jan Herec (xherec00)
 * Datum: 16. března 2017
 * Soubor: server.cpp
 * Komentar: Zdrojový soubor představující implementaci serveru, který bude komunikovat s klientem pomocí zabezpečeného tunelu
 * Kodovani: UTF-8
 */ 

#include <stddef.h>
#include <stdlib.h>
#include <stdio.h>
#include <cstdlib>
#include <unistd.h>
#include <string>
#include <vector>
#include <iostream>
#include <fstream>
#include <sys/types.h>
#include <new>   
#include <deque>
#include <math.h> 
#include <limits>
#include <string.h>
#include <time.h>

#include <sys/stat.h>
#include <fcntl.h>
#include <errno.h>

#include <openssl/conf.h>
#include <openssl/evp.h>
#include <openssl/err.h>
#include <openssl/dh.h>
#include <openssl/bn.h>

#include <gmp.h>

#include "server.hpp"
#include "client.hpp"
#include "client_server_common.hpp"

using namespace std;

// ########################## PROTOTYPY FUNKCÍ #################################

/**
 * Funkce představuje životní cyklus serveru (přijímá zprávy od autentizovaného klienta přes zabezpečený tunel, po přijetí všech zpráv končí)
 */
void startServer();

/**
 * Tato poněkud delší funkce (předávání pole typu mpz_t při volání funkce se ukázalo jako problém, proto vnitřek této funkce není dekomponován)
 * představuje implementaci feigeFiatShamir identifikačního schématu, při kterém se klient autentizuje serveru
 * @param secretKeyReduced tajný 256b klíč, pro symetrickou kryptografickou šifru AES vypočítaný a redukovaný z DH
 * @param iv inicilaizační 128b vektor použitý při CBC režimu AES, získaný redukcí z DH
 * @param write_in_pipe_from_server_to_client deskriptor otevřené pojmenované roury pro zápis (směr server -> klient)
 * @param read_from_pipe_from_client_to_server deskriptor otevřené pojmenované roury pro čtení (směr klient -> server)
 * @return 0, pokud autentizace klienta proběhla úspěšně, jinak -1 při jakékoliv chybě
 */
int feigeFiatShamirClientIdentification(unsigned char* secretKeyReduced, unsigned char* iv, int write_in_pipe_from_server_to_client, int read_from_pipe_from_client_to_server);

/**
 * Funkce postupně přijímá zprávy od klienta v zašifrované podobě a odesílá hash zprávy klientovi pro potvrzení integrity
 * @param secretKeyReduced tajný 256b klíč, pro symetrickou kryptografickou šifru AES vypočítaný a redukovaný z DH
 * @param iv inicilaizační 128b vektor použitý při CBC režimu AES, získaný redukcí z DH
 * @param write_in_pipe_from_server_to_client deskriptor otevřené pojmenované roury pro zápis (směr server -> klient)
 * @param read_from_pipe_from_client_to_server deskriptor otevřené pojmenované roury pro čtení (směr klient -> server)
 * @return 0, pokud se podařilo přijmou všechny zprávy od klienta, jinak -1 při jakékoliv chybě
 */
int receiveMessagesFromClient(unsigned char* secretKeyReduced, unsigned char* iv, int write_in_pipe_from_server_to_client, int read_from_pipe_from_client_to_server);


// ######################### DEFINICE FUNKCÍ ###################################

/**
 * Funkce představuje životní cyklus serveru (přijímá zprávy od autentizovaného klienta přes zabezpečený tunel, po přijetí všech zpráv končí)
 */
void startServer() {
    
    openSSLInit();
    
    DH *publicPrivateKeyPair; // DH: soukromý a veřejný klíč jako pár
    
    //********* DH ALGORITMUS: GENEROVANI PARU KLICU ***********
    
    // vygenerovani soukromeho a verejenho klice jako paru
    if (generatePublicPrivateKeyPair(&publicPrivateKeyPair) == -1) {
        fprintf(stderr, "Cyhba při generování páru klíčů\n");
        ERR_print_errors_fp(stderr);
        exit(EXIT_FAILURE);
    }
    
    // převod veřejného klíče do hexadecimální textové reprezentace
    char *publicKeyInHex = BN_bn2hex(publicPrivateKeyPair->pub_key);
    if (publicKeyInHex == NULL) {
        fprintf(stderr, "Chyba při převodu veřejného klíče do hexadecimální textové reprezentace:  %s\n");
        ERR_print_errors_fp(stderr);
        exit(EXIT_FAILURE);
    }


    // BN_print_fp(stdout, publicPrivateKeyPair->pub_key);
    // fflush(stdout);
    // odstraníme pojmenované roury, pokud by byly náhodou otevřené
    unlink(NAMED_PIPE_FROM_SERVER_TO_CLIENT);
    unlink(NAMED_PIPE_FROM_CLIENT_TO_SERVER);
    
    
    int read_from_pipe_from_client_to_server; // deskriptor otevřené pojmenované roury pro zápis (směr server -> klient)
    int write_in_pipe_from_server_to_client; // deskriptor otevřené pojmenované roury pro čtení (směr klient -> server)
  
    // vytvoříme pojmenovanou rouru pro komunikaci ve směru: klient -> server
    if (mkfifo(NAMED_PIPE_FROM_CLIENT_TO_SERVER, 0666) == -1) {
        fprintf(stderr, "Nebylo možné vytvořit pojmenovanou rouru:  %s\n", NAMED_PIPE_FROM_CLIENT_TO_SERVER);
        perror("");           
        unlink(NAMED_PIPE_FROM_CLIENT_TO_SERVER);
        exit(EXIT_FAILURE);
    }

    // vytvoříme pojmenovanou rouru pro komunikaci ve směru: server -> klient
    else if (mkfifo(NAMED_PIPE_FROM_SERVER_TO_CLIENT, 0666) == -1) {
        fprintf(stderr, "Nebylo možné vytvořit pojmenovanou rouru:  %s\n", NAMED_PIPE_FROM_SERVER_TO_CLIENT);
        perror("");
        unlink(NAMED_PIPE_FROM_SERVER_TO_CLIENT);
        unlink(NAMED_PIPE_FROM_CLIENT_TO_SERVER);
        exit(EXIT_FAILURE);
    }

    // otevření pojmenované roury pro čtení (směr klient -> server), otevření je blokující, server je tedy třeba nastartovat jako první a ten čeká až se s ním klient spojí
    if ((read_from_pipe_from_client_to_server = open(NAMED_PIPE_FROM_CLIENT_TO_SERVER, O_RDONLY)) == -1) {
        fprintf(stderr, "Nebylo možné otevřít pro čtení pojmenovanou rouru:  %s\n", NAMED_PIPE_FROM_CLIENT_TO_SERVER);
        perror("");
        unlink(NAMED_PIPE_FROM_SERVER_TO_CLIENT);
        unlink(NAMED_PIPE_FROM_CLIENT_TO_SERVER);
        exit(EXIT_FAILURE);
    }
    // otevření pojmenované roury pro zápis (směr server -> klient)
    else if ((write_in_pipe_from_server_to_client = open(NAMED_PIPE_FROM_SERVER_TO_CLIENT, O_WRONLY)) == -1) {
        fprintf(stderr, "Nebylo možné otevřít pro zápis pojmenovanou rouru:  %s\n", NAMED_PIPE_FROM_SERVER_TO_CLIENT);
        perror("");
        close(read_from_pipe_from_client_to_server);
        unlink(NAMED_PIPE_FROM_SERVER_TO_CLIENT);
        unlink(NAMED_PIPE_FROM_CLIENT_TO_SERVER);
        exit(EXIT_FAILURE);
    }

    //********* DH ALGORITMUS: VYMENA VERJENYCH KLICU PROTISTRAN ***********
    
    // poslani verejneho klice klientovi přes pojmenovanou rouru
    if (sendPublicKeyToPeer(publicKeyInHex, write_in_pipe_from_server_to_client) == -1) {
        fprintf(stderr, "Chyba při zasílání veřejného klíče přes rouru:  %s. \n", NAMED_PIPE_FROM_SERVER_TO_CLIENT);
        perror("");
        ERR_print_errors_fp(stderr);
        close(write_in_pipe_from_server_to_client);
        close(read_from_pipe_from_client_to_server);
        unlink(NAMED_PIPE_FROM_SERVER_TO_CLIENT);
        unlink(NAMED_PIPE_FROM_CLIENT_TO_SERVER);
        exit(EXIT_FAILURE);
    }
    
    
    // ziskani verejneho klice klienta přes pojemnovanou rouru
    BIGNUM *publicKey = NULL;
    if (receivePublicKeyFromPeer(read_from_pipe_from_client_to_server, &publicKey) == -1) {
        fprintf(stderr, "Chyba při čtení veřejného klíče přes rouru:  %s. \n", NAMED_PIPE_FROM_CLIENT_TO_SERVER);
        perror("");
        ERR_print_errors_fp(stderr);
        close(write_in_pipe_from_server_to_client);
        close(read_from_pipe_from_client_to_server);
        unlink(NAMED_PIPE_FROM_SERVER_TO_CLIENT);
        unlink(NAMED_PIPE_FROM_CLIENT_TO_SERVER);
        exit(EXIT_FAILURE);
    }

    //********* DH ALGORITMUS: VYPOCET SDILENEHO TAJEMSTVI ***********
    
    // vypocet sdileneho tajemstvi o 2048bitech
    unsigned char *secretKey;
    int secretSize;
    if ((secretSize = computeSharedSecret(&secretKey, publicKey, publicPrivateKeyPair)) == -1) {
        fprintf(stderr, "Chyba při výpočtu sdíleného tajemství. \n");
        ERR_print_errors_fp(stderr);
        close(write_in_pipe_from_server_to_client);
        close(read_from_pipe_from_client_to_server);
        unlink(NAMED_PIPE_FROM_SERVER_TO_CLIENT);
        unlink(NAMED_PIPE_FROM_CLIENT_TO_SERVER);
        exit(EXIT_FAILURE);
    }

   //printf("The shared secret is:\n");
    //BIO_dump_fp(stdout, (const char*) secret, secretSize);
    
    // ziskani redukovaneho klice na 256 bitů z puvodniho klice (sdileneho tajemstvi)
    unsigned char *secretKeyReduced;
    if ((secretKeyReduced = reduce2048BitKeyTo256bits(secretKey)) == NULL) {
        fprintf(stderr, "Chyba při výpočtu redukovaného sdíleného tajného klíče. \n");
        close(write_in_pipe_from_server_to_client);
        close(read_from_pipe_from_client_to_server);
        unlink(NAMED_PIPE_FROM_SERVER_TO_CLIENT);
        unlink(NAMED_PIPE_FROM_CLIENT_TO_SERVER);
        exit(EXIT_FAILURE);
    }
    
    // ziskani IV (inicializacniho vektoru) o 128 bitech z puvodniho neredukovaneno sdileneho klice
    unsigned char *iv;
    if ((iv = get128BitIVFrom2048BitKey(secretKey)) == NULL) {
        fprintf(stderr, "Chyba při výpočtu inicializačního vektoru ze sdíleného tajného klíče. \n");
        close(write_in_pipe_from_server_to_client);
        close(read_from_pipe_from_client_to_server);
        unlink(NAMED_PIPE_FROM_SERVER_TO_CLIENT);
        unlink(NAMED_PIPE_FROM_CLIENT_TO_SERVER);
        exit(EXIT_FAILURE);
    }
        
    printf("\n\nKOMUNIKAČNÍ FÁZE 1: AUTENTIZACE KLIENTA POMOCÍ FEIGE-FIAT-SHAMIR\n\n");
    // provedeme autentizaci klienta vůči serveru pomocí feige-fiat-shamira
    if (feigeFiatShamirClientIdentification(secretKeyReduced, iv, write_in_pipe_from_server_to_client, read_from_pipe_from_client_to_server) == -1) {
        close(write_in_pipe_from_server_to_client);
        close(read_from_pipe_from_client_to_server);
        unlink(NAMED_PIPE_FROM_SERVER_TO_CLIENT);
        unlink(NAMED_PIPE_FROM_CLIENT_TO_SERVER);
        exit(EXIT_FAILURE);
    }
    
    printf("\n\nKOMUNIKAČNÍ FÁZE 2: ČTENÍ ZPRÁV OD KLIENTA\n\n");
    // postupně přečteme všechny zprávy od klienta (dokud nepošle spciální ukončovací zprávu END) v zašifrované podobě a odesíláme hash zprávy klientovi pro potvrzení integrity
    if (receiveMessagesFromClient(secretKeyReduced, iv, write_in_pipe_from_server_to_client, read_from_pipe_from_client_to_server) == -1) {
        close(write_in_pipe_from_server_to_client);
        close(read_from_pipe_from_client_to_server);
        unlink(NAMED_PIPE_FROM_SERVER_TO_CLIENT);
        unlink(NAMED_PIPE_FROM_CLIENT_TO_SERVER);
        exit(EXIT_FAILURE);
    }
    
    printf("Přijata zpráva, která ukončuje činnost serveru.\n\n");

    close(read_from_pipe_from_client_to_server);
    close(write_in_pipe_from_server_to_client);
    
    unlink(NAMED_PIPE_FROM_SERVER_TO_CLIENT);
    unlink(NAMED_PIPE_FROM_CLIENT_TO_SERVER);
    
    openSSLcleanUp();
}

/**
 * Tato poněkud delší funkce (předávání pole typu mpz_t při volání funkce se ukázalo jako problém, proto vnitřek této funkce není dekomponován)
 * představuje implementaci feigeFiatShamir identifikačního schématu, při kterém se klient autentizuje serveru
 * Implementace vychází z knihy Handbook of Applied Cryptography
 * @param secretKeyReduced tajný 256b klíč, pro symetrickou kryptografickou šifru AES vypočítaný a redukovaný z DH
 * @param iv inicilaizační 128b vektor použitý při CBC režimu AES, získaný redukcí z DH
 * @param write_in_pipe_from_server_to_client deskriptor otevřené pojmenované roury pro zápis (směr server -> klient)
 * @param read_from_pipe_from_client_to_server deskriptor otevřené pojmenované roury pro čtení (směr klient -> server)
 * @return 0, pokud autentizace klienta proběhla úspěšně, jinak -1 při jakékoliv chybě
 */
int feigeFiatShamirClientIdentification(unsigned char* secretKeyReduced, unsigned char* iv, int write_in_pipe_from_server_to_client, int read_from_pipe_from_client_to_server) {
    // deklarace proměnných související s komunikací mezi klientem a serverem    
    unsigned char receivedPlainMessage[1000]; // dešiforavaná plain text zpráva od klienta, posilame si velka čísla, takže 1000 číslic na reprezentaci by mělo stačit 
    char receivedPlainMessageAsString[1001]; // kvůli vypsání přijaté zprávy bude potřeba pomocné pole, kam zprávu překopírujeme a nakonec dáem ukončovatč O 
    
    unsigned char* receivedCipherMessage; // přijatá zašifrovaná zpráva od klienta
    int receivedCipherMessageLength; // délka přijaté zašifrované zprávy od klienta
    int decryptedMessageLength; // délka rozšifrované zprávy (plaintextu) od klienta
    
    unsigned char *plainMessage; // otevřená zpráva určená k zaslání na klienta, případně určená pro pomocné výpiy
    unsigned char cipherMessage[1000]; // otevřená zpráva určená k zaslání na klienta, posilame si velka čísla, takže 1000 číslic na reprezentaci by mělo stačit 
    int cipherMessageLength;  // délka zašifrované zprávy určené k zaslání na klienta
    
    // Handbook of Applied Cryptography, kap.: 10.4.2, krok: 1.
    // veřejný modulus
    const char *n_str = "85607608361883070139487072005518234154391303259258668170145277603626072507259700154004829969474114946831113599352530071726597191105663477559589305326363051448095733041144989668728120597735377089561152540467595768457656273919356340661823782745226015315325327802612307805861506456951670332607726320254080991848078142552936732279938665522177348648676380017215567994445699527398794660984869453217645658350234339734872965211517440358583367321462505077775617008847199375021975475799691131050049268425189591656036446643350938674629581875311006301181987909195136913240466613603845076721915675800881019187412013581285159001047";
    
    // vektor integerů v (veřejný klíč klienta)
    const char *v_str_vector[feigeFiatShamir_K_VALUE];
    v_str_vector[0] = "38213180427773519381176465589346235989803669475868358682255009865754871465050757383016869988228922617495997862538498499022137879392755658617362328792406896442278788440711309749325729128887293790852510117861895636311478143819706245511343279297550048036623372756951876649682105638522455018174656759651521022403984453368594407481484884073494918461389332631697952555086826672060248360390487554370320865528569139604089164285997905972986547880178799494594972742244081052105320719927164523373127260542340220122444836766208663789888509799107128243518879421638936562081020781114172587051423774239675543272055763995326323027408";
    v_str_vector[1] = "53442692748458494712953853599070254602440991737167029092204137882422140938269754907983038791258577405130464963036366714742585475056080673626449016451554788525339241557787239750505960546221567167483626499852791260171605492459363647683514521344375037852428805827971165727296621045646692174468647120226571738271067706135052963154136909707338066786883045869103502997705844663280155399386570838465260966872191479504081343473976849537516907268122750184163964800671881889237780286197912864366996500757096678002692952198202542184103949707294990480649000822787249498958686128081775728162744373816328080842619569817261707609520";
    v_str_vector[2] = "10831851475475091754968713525937409147469523647868040888856098773412634304960455776325024345356136141307958247758638299525741195652440181627353942551375877602103465500256983147202097694163515734532257690968177014796502428480939386135102492296684116472444868621920729168557229426582041719062339871402957881844173701948548828166841615449637452982070185649936176035573897606715486127898754572209896846911802656696936788259178076919365364776271651815936625682528515789787857842381731199691206513398878452768322305812844556376708204321697864835981443666309563885417457224679098712719353421202046163531622077130970700709033";
    v_str_vector[3] = "64076114624823417061281156924443232735683836141707032042637540341462410745020661773824225582687217665444896174107289727488805897042758939464644676501077641862555225794832772331763446956653278045308072082159323301687345777854303367940132595249761450048550898017569954223018895080567099074234873610916180102419779674509997065751761666351974741373635365508663784564943431748957960356908833789430631268403855378173113449726824892612660715153593105713490832557018443464752166953834265726063680945993343891054384308001824912220978140765713579694734107215870556328799194245416392070448026939930022442821395881595966287332553";
    v_str_vector[4] = "56490000487831572828582956034884433428634771695759035103665948374489817383234565955918627169804067266442368278705275831117508791666561121116556648974401043093286185339190518878226241975290870786341114374849684866989700587675060608203378689940602909461341736585021350284289543486708400058231386671468671157337205684427329620853081876755588350957700469249382488879321289122879825576843542199173717404368112957063600858571565450043019893694005950058995344946587103050027946520699654140018731840768033539444971975656569797248467373780988212798579846163842920949244286637816827157805837064571628197774304070538343077638658";
    
    // deklarace vektorů a proměnných
    mpz_t n, x, y, z; // proměnné z algoritmu feigeFiatShamir
    mpz_t randNumber, constant2, tmp, tmp2, constantMinus1, constant10; // pomocné porměnné
    gmp_randstate_t randState; // promenna pro generovani nahodnych cisel
    
    // vektory z algoritmu feigeFiatShamir
    mpz_t v_vector[feigeFiatShamir_K_VALUE];
    mpz_t e_vector[feigeFiatShamir_K_VALUE]; // challenge vektor
    
    // iniclialiazce vektorů a proměnných
    for (int i = 0; i < feigeFiatShamir_K_VALUE; i++) {
        mpz_init(v_vector[i]);
        mpz_init(e_vector[i]);
    }
    mpz_init(n); mpz_init(y); mpz_init(x); mpz_init(z); mpz_init(randNumber);mpz_init(constant2); mpz_init(constantMinus1); mpz_init(constant10); mpz_init(tmp); mpz_init(tmp2);
    gmp_randinit_mt(randState);
    
    // nastavený známých hodnot proměnným
    for (int i = 0; i < feigeFiatShamir_K_VALUE; i++) {
        mpz_set_str(v_vector[i], v_str_vector[i], 10);
    }
    mpz_set_str(n,n_str, 10);
    mpz_set_si(constant2, 2);
    gmp_randseed_ui (randState, time(0));
    
    bool autentization = false; // indikuje jestli autentozace proebehla uspesne
    
    // Handbook of Applied Cryptography, kap.: 10.4.2, krok: 4. několikrát zopakovaný
    // provedem několik kol ověření, abychom zvýšili pravděpodobnost jistotu identity protistrany
    for (int i = 0; i < feigeFiatShamir_T_ROUNDS; i++) {
        printf("\n%d kolo autentizace:\n\n", i );
        
        // Handbook of Applied Cryptography, kap.: 10.4.2, krok: 4. (a)
        // ****** ZISKANI X OD KLIENTA ******
        
        // přečteme zprávu od klienta (v zašifrované podobě)
        if ((receivedCipherMessageLength = receiveCipherMessageFromPeer(read_from_pipe_from_client_to_server, &receivedCipherMessage)) == -1) {
            fprintf(stderr, "Chyba při čtení zprávy od klienta přes rouru:  %s. \n", NAMED_PIPE_FROM_CLIENT_TO_SERVER);
            perror("");
            return -1;
        }
        
        // rozšifrujeme zprávu
        if ((decryptedMessageLength = decrypt(receivedCipherMessage, receivedCipherMessageLength, secretKeyReduced, iv, receivedPlainMessage)) == -1) {
            fprintf(stderr, "Chyba:  Nepodařilo se rozšifrovat zprávu od klienta\n");
            ERR_print_errors_fp(stderr);
            return -1;
        }
        
        // převedeme přijatou pzrávu na řětezec a vypíšeme přijatou pzrávu
        for (int i = 0; i < decryptedMessageLength;i++) {
            receivedPlainMessageAsString[i] = (char) receivedPlainMessage[i];
            if (i == decryptedMessageLength - 1) {
                receivedPlainMessageAsString[i + 1] = '\0';
            }
        }
        printf("Přijato od klienta x: %s\n", receivedPlainMessageAsString);
        mpz_set_str(x, receivedPlainMessageAsString, 10); // uložíme x přijateé od klienta
        
        free(receivedCipherMessage);
        
        // Handbook of Applied Cryptography, kap.: 10.4.2, krok: 4. (b)
        // ******* POŠLEME KLIENTOVI CHALLENGE VEKTOR E *******
        
        // vypočítáme vektor náhodných bitů e
        for (int i = 0; i < feigeFiatShamir_K_VALUE; i++) {
            mpz_urandomm (randNumber, randState, constant2);
            mpz_set(e_vector[i], randNumber);
        }
                
        printf("Zaslání challenge vektoru klientovi. \n");
        // zašleme jednotlivé složky vektoru e na server
        for (int i = 0; i < feigeFiatShamir_K_VALUE;i++) {
            plainMessage = (unsigned char*) mpz_get_str (NULL, 10, e_vector[i]); // převedeme číslo e_i na řetězec
            printf("Zasílám klientovi e%d: %s\n", i, plainMessage);

            // zašifrujeme zprávu
            if ((cipherMessageLength = encrypt (plainMessage, strlen ((char *)plainMessage), secretKeyReduced, iv, cipherMessage)) == -1) {
                fprintf(stderr, "Chyba:  Nepodařilo se zašifrovat zprávu:  %s. \n", plainMessage);
                ERR_print_errors_fp(stderr);
                return -1;
            }

            // zašleme zprávu klientovi
            if (sendCipherMessageToPeer(cipherMessage, cipherMessageLength, write_in_pipe_from_server_to_client) == -1) {
                fprintf(stderr, "Chyba:  Chyba při zasílání zprávy na server přes rouru:  %s. \n", NAMED_PIPE_FROM_SERVER_TO_CLIENT);
                perror("");
                return -1;
            }

            free(plainMessage);

        }
        
        // Handbook of Applied Cryptography, kap.: 10.4.2, krok: 4. (c)
        // ****** ZISKANI Y OD KLIENTA ******

        // přečteme zprávu od klienta (v zašifrované podobě)
        if ((receivedCipherMessageLength = receiveCipherMessageFromPeer(read_from_pipe_from_client_to_server, &receivedCipherMessage)) == -1) {
            fprintf(stderr, "Chyba při čtení zprávy od klienta přes rouru:  %s. \n", NAMED_PIPE_FROM_CLIENT_TO_SERVER);
            perror("");
            return -1;
        }
        
        // rozšifrujeme zprávu
        if ((decryptedMessageLength = decrypt(receivedCipherMessage, receivedCipherMessageLength, secretKeyReduced, iv, receivedPlainMessage)) == -1) {
            fprintf(stderr, "Chyba:  Nepodařilo se rozšifrovat zprávu od klienta\n");
            ERR_print_errors_fp(stderr);
            return -1;
        }
        
        // převedeme přijatou pzrávu na řětezec a vypíšeme přijatou pzrávu
        for (int i = 0; i < decryptedMessageLength;i++) {
            receivedPlainMessageAsString[i] = (char) receivedPlainMessage[i];
            if (i == decryptedMessageLength - 1) {
                receivedPlainMessageAsString[i + 1] = '\0';
            }
        }
        printf("Přijato od klienta y: %s\n", receivedPlainMessageAsString);
        mpz_set_str(y, receivedPlainMessageAsString, 10); // uložíme y přijateé od klienta
        
        free(receivedCipherMessage);
        
        // Handbook of Applied Cryptography, kap.: 10.4.2, krok: 4. (d)
        // ****** VYPOČÍTÁME Z A OVĚŘÍME JESTLI DANÉ KOLO AUTENTOZACE PROBĚHLO ÚSPĚŠNĚ ******
        
        // vypocitame z
        mpz_set_si(tmp, 1); // inicializace akumulacni promenne
        for (int i = 0; i < feigeFiatShamir_K_VALUE;i++) {
            mpz_powm (tmp2, v_vector[i], e_vector[i], n); // vypocet tmp2 = (v_i^(e_i)
            mpz_mul (tmp, tmp, tmp2);                     // vypocet tmp *= tmp2
        }
        mpz_pow_ui(tmp2, y, 2);   // vypocet (y^2)
        mpz_mul (tmp, tmp2, tmp); // y^2 * pi(v_j^(e_j))
        mpz_mod (z, tmp, n);      // vypocet z = y^2 * pi(v_j^(e_j)) mod n
        
        // vyúíšeme vypočítané z
        plainMessage = (unsigned char*) mpz_get_str (NULL, 10, z); // zašleme;
        printf("Z = %s\n", plainMessage);
        free(plainMessage);
        
        // a porovnam z s x, abychom zjistili jestli se autentizace podarila
        autentization = false;
        if (mpz_cmp_ui(z, 0) != 0) {
            mpz_neg(tmp, x);       //-x
            mpz_mod (tmp, tmp, n); // na -x aplikujeme modulo n
            // pokud z = +-x a z != 0, pak toto kolo autentizace probehlo uspesne
            if (mpz_cmp(z, x) == 0 || mpz_cmp(z, tmp) == 0 ) {
                autentization = true;
            }
        }
        
        // kolo autentiozace neproběhlo úspěšně tedy celá autentozae neproběhla úsúěšně
        if (autentization == false) {
            fprintf(stderr,"Autentizace klienta pomocí feige Fiat Shamir shématu neproběhla úspěšně, ukončuji spojení s falešným klientem.\n");
            return -1;
        }
                
    }
    
    // autentiztace proběhla úspěšně informujem otom klienta
    printf("Autentizace klienta pomocí feige Fiat Shamir shématu proběhla úspěšně\n");
    plainMessage = (unsigned char*) "AUTHENTIZATION SUCCESS"; // zašleme;

    // zašifrujeme zprávu
    if ((cipherMessageLength = encrypt(plainMessage, strlen((char *) plainMessage), secretKeyReduced, iv, cipherMessage)) == -1) {
        fprintf(stderr, "Chyba:  Nepodařilo se zašifrovat zprávu:  %s. \n", plainMessage);
        ERR_print_errors_fp(stderr);
        return -1;
    }

    // zašleme zprávu KLIENTOVI
    if (sendCipherMessageToPeer(cipherMessage, cipherMessageLength, write_in_pipe_from_server_to_client) == -1) {
        fprintf(stderr, "Chyba:  Chyba při zasílání zprávy na server přes rouru:  %s. \n", NAMED_PIPE_FROM_SERVER_TO_CLIENT);
        perror("");
        return -1;
    }
    
    // uvolnění paměti
    for (int i = 0; i < feigeFiatShamir_K_VALUE; i++) {
        mpz_clear(v_vector[i]);
        mpz_clear(e_vector[i]);
    }

    mpz_clear(n);  mpz_clear(x); mpz_clear(y); mpz_clear(z);mpz_clear(randNumber); mpz_clear(constant2);
    
}

/**
 * Funkce postupně přijímá zprávy od klienta v zašifrované podobě a odesílá hash zprávy klientovi pro potvrzení integrity
 * @param secretKeyReduced tajný 256b klíč, pro symetrickou kryptografickou šifru AES vypočítaný a redukovaný z DH
 * @param iv inicilaizační 128b vektor použitý při CBC režimu AES, získaný redukcí z DH
 * @param write_in_pipe_from_server_to_client deskriptor otevřené pojmenované roury pro zápis (směr server -> klient)
 * @param read_from_pipe_from_client_to_server deskriptor otevřené pojmenované roury pro čtení (směr klient -> server)
 * @return 0, pokud se podařilo přijmou všechny zprávy od klienta, jinak -1 při jakékoliv chybě
 */
int receiveMessagesFromClient(unsigned char* secretKeyReduced, unsigned char* iv, int write_in_pipe_from_server_to_client, int read_from_pipe_from_client_to_server) {
        
    unsigned char cipherMessage[100];  // otevřená zpráva určená k zaslání na klienta
    unsigned char receivedPlainMessage[100]; // dešiforavaná plain text zpráva od klienta, v praxi to bude hash, který zasílá server 256bitový, stačilo by tedy 32 bajtů, ale z hlediska škálování je lepší nechat větší prostor
    char receivedPlainMessageAsString[101]; //kvůli vypsání přijaté zprávy bude potřeba pomocné pole, kam zprávu překopírujeme a nakonec dáem ukončovatč O 
    
    unsigned char* receivedCipherMessage; // přijatá zašifrovaná zpráva od klienta
    int receivedCipherMessageLength; // délka přijaté zašifrované zprávy od klienta
    int cipherMessageLength; // délka zašifrované zprávy určené k zaslání na klienta
    int decryptedMessageLength; // délka rozšifrované zprávy (plaintextu) od klienta
    
    unsigned char* digestOfMessage;    // hash přijaté zprávy
    unsigned int digestOfMessageLength; //délka hashe přijaté zprávy (měla by být vždy 256)
    
    // postupně přečteme všechny zprávy od klienta v zašifrované podobě a odesíláme hash zprávy klientovi pro potvrzení integrity
    while (1) {
        
        // přečteme zprávu od klienta (v zašifrované podobě)
        if ((receivedCipherMessageLength = receiveCipherMessageFromPeer(read_from_pipe_from_client_to_server, &receivedCipherMessage)) == -1) {
            fprintf(stderr, "Chyba při čtení zprávy od klienta přes rouru:  %s. \n", NAMED_PIPE_FROM_CLIENT_TO_SERVER);
            perror("");
            return -1;
        }
        
        // rozšifrujeme zprávu
        if ((decryptedMessageLength = decrypt(receivedCipherMessage, receivedCipherMessageLength, secretKeyReduced, iv, receivedPlainMessage)) == -1) {
            fprintf(stderr, "Chyba:  Nepodařilo se rozšifrovat zprávu od klienta\n");
            ERR_print_errors_fp(stderr);
            return -1;
        }
        
        // převedeme přijatou pzrávu na řětezec a vypíšeme přijatou pzrávu
        for (int i = 0; i < decryptedMessageLength;i++) {
            receivedPlainMessageAsString[i] = (char) receivedPlainMessage[i];
            if (i == decryptedMessageLength - 1) {
                receivedPlainMessageAsString[i + 1] = '\0';
            }
        }
        printf("Přijata zpráva od klienta: %s\n", receivedPlainMessageAsString);
        
        // vytvoříme hash přijaté zprávy
        if (digestMessageWithSHA256(receivedPlainMessage, decryptedMessageLength, &digestOfMessage, &digestOfMessageLength) == -1) {
            fprintf(stderr, "Chyba:  Nepodařilo se vytvořit hash zprávy:  %s. \n", receivedPlainMessageAsString);
            ERR_print_errors_fp(stderr);
            return -1;
        }
        
        // zašifrujeme hash zprávy zprávu
        if ((cipherMessageLength = encrypt (digestOfMessage, digestOfMessageLength, secretKeyReduced, iv, cipherMessage)) == -1) {
            fprintf(stderr, "Chyba:  Nepodařilo se zašifrovat hash zprávy:  %s. \n", receivedPlainMessageAsString);
            ERR_print_errors_fp(stderr);
            return -1;
        }
        
        // zašleme zašifrovaný hash zprávy klientovi
        if (sendCipherMessageToPeer(cipherMessage, cipherMessageLength, write_in_pipe_from_server_to_client) == -1) {
            fprintf(stderr, "Chyba:  Chyba při zasílání hashe zprávy klientovi přes rouru:  %s. \n", NAMED_PIPE_FROM_SERVER_TO_CLIENT);
            perror("");

            return -1;
        }
        
        
        printf("Poslán hash přijaté zprávy klientovi pro ověření integrity\n\n");
        
        // pokud nám klient zaslal poslední zprávu, ukončíme činnost serveru
        if (strcmp(receivedPlainMessageAsString, "END") == 0) break;
        
    }
}