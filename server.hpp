/*
 * Autor: Jan Herec (xherec00)
 * Datum: 16. března 2017
 * Soubor: server.hpp
 * Komentar: Hlavičkový soubor představující implementaci serveru, který bude komunikovat s klientem pomocí zabezpečeného tunelu
 * Kodovani: UTF-8
 */ 

#ifndef SERVER_HPP
#define SERVER_HPP

// jméno pojmenované roury určené ke komunikaci ve směru: klient -> server
#define NAMED_PIPE_FROM_CLIENT_TO_SERVER "/tmp/xherec00_KRY_NAMED_PIPE_FROM_CLIENT_TO_SERVER"

/**
 * Funkce představuje životní cyklus serveru (přijímá zprávy od autentizovaného klienta přes zabezpečený tunel, po přijetí všech zpráv končí)
 */
void startServer();


#endif /* SERVER_HPP */
