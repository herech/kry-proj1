/*
 * Autor: Jan Herec (xherec00)
 * Datum: 31. března 2017
 * Soubor: main.cpp
 * Komentar: Aplikace, se spustí bud jako server (přepínač -s) nebo klient (přepínač -c) a bude komunikovat s protistranou pomocí zabezpečeného tunelu
 * Kodovani: UTF-8
 */ 

#include <stdlib.h>
#include <stdio.h>
#include <cstdlib>
#include <unistd.h>
#include <string>
#include <vector>
#include <getopt.h>
#include <iostream>
#include <fstream>

#include "server.hpp"
#include "client.hpp"

using namespace std;

// ##################### DEFINICE DATOVÝCH STRUKTUR ############################

/**
 * Mód programu (klient nebo server)
 */
enum Mode {Client, Server, NoMode};

/* Struktura, ktera obsahuje zpracovane vstupní parametry programu */
typedef struct{
    Mode mode; // mód programu	
} tProcessedParameters;

// ########################## PROTOTYPY FUNKCÍ #################################

/**
 * Funkce pro zpracování vstupních parametrů 
 * @param argc počet vstupních argumentů
 * @param argv vstupní argumenty
 * @param processedParameters výstupní parametr, který představuje strukturu se zpracovanými parametry
 */
void processTheParams(int argc, char **argv, tProcessedParameters* processedParameters);

/**
 * Klasicka funkce main, ktera realizuje cely program volanim podprogramu
 * @param argc počet vstupních argumentů
 * @param argv pole vstupních argumentů
 * @return -1 při chybě, jinak 0, pokud aplikace skončila standardně
 */
int main(int argc, char **argv);

// ######################### DEFINICE FUNKCÍ ###################################

/**
 * Funkce pro zpracování vstupních parametrů 
 * @param argc počet vstupních argumentů
 * @param argv vstupní argumenty
 * @param processedParameters výstupní parametr, který představuje strukturu se zpracovanými parametry
 */
void processTheParams(int argc, char **argv, tProcessedParameters* processedParameters) {
    Mode mode = NoMode; // v jakém módu má běžet program
    
    int param;  // aktuální načtený vstupní parametr
    opterr = 0; // vypneme výchozí výpis chyb v souvoslosti s parsování vstupních parametrů
    while ((param = getopt(argc, argv,"cs")) != -1) {
        switch (param) {
            // parametr -c pro mód porgramu klient
            case 'c' : 
                if (mode != NoMode) {
                    fprintf(stderr, "\nChyba: Chyba při zpracování vstupních parametrů! Bylo zadáno více módů programu! \n");
                    exit(EXIT_FAILURE);
                }
                mode = Client;
                break;
            // parametr -s pro mód programu server
            case 's' : 
                if (mode != NoMode) {
                    fprintf(stderr, "\nChyba: Chyba při zpracování vstupních parametrů! Bylo zadáno více módů programu! \n");
                    exit(EXIT_FAILURE);
                }
                mode = Server;
                break;
            // při chybě
            default: 
                fprintf(stderr, "\nChyba: Chyba při zpracování vstupních parametrů! Byly zadány nesprávné parametry. \n");
                exit(EXIT_FAILURE);
        }
    }
    
    // pokud nebyl zadán žádný mód programu, je to chyba
    if (mode == NoMode) {
        fprintf(stderr, "\nChyba: Chyba při zpracování vstupních parametrů! Nebyl zadán žádný mód programu.\n");
        exit(EXIT_FAILURE);
    }
    
    // nastavíme parametry ve struktuře, která je výstupním parametrem
    processedParameters->mode = mode;	
}

/**
 * Klasicka funkce main, ktera realizuje cely program volanim podprogramu
 * @param argc počet vstupních argumentů
 * @param argv pole vstupních argumentů
 * @return -1 při chybě, jinak 0, pokud aplikace skončila standardně
 */
int main(int argc, char **argv)
{
    // zpracujeme vstupní parametry
    tProcessedParameters processedParameters;
    processTheParams(argc, argv, &processedParameters);
    
    // nastartujeme klienta nebo server
    if (processedParameters.mode == Client) {
        startClient();
    }
    else if (processedParameters.mode == Server) {
        startServer();
    }
    
    return 0;
            
}


	
