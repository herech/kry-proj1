/*
 * Autor: Jan Herec (xherec00)
 * Datum: 16. března 2017
 * Soubor: client.cpp
 * Komentar: Zdrojový soubor představující implementaci klienta, který bude komunikovat se serverem pomocí zabezpečeného tunelu
 * Kodovani: UTF-8
 */ 

#include <stddef.h>
#include <stdlib.h>
#include <stdio.h>
#include <cstdlib>
#include <unistd.h>
#include <string>
#include <vector>
#include <iostream>
#include <fstream>
#include <sys/types.h>
#include <new>   
#include <deque>
#include <math.h> 
#include <limits>
#include <string.h>
#include <time.h>

#include <sys/stat.h>
#include <fcntl.h>
#include <errno.h>

#include <openssl/conf.h>
#include <openssl/evp.h>
#include <openssl/err.h>
#include <openssl/dh.h>
#include <openssl/bn.h>

#include <gmp.h>

#include "server.hpp"
#include "client.hpp"
#include "client_server_common.hpp"

// počet zpráv/dotazů zaslaných na server (včetně ukončovacího dotazu/zprávy END)
#define NUM_OF_MESSAGES 6

using namespace std;

// ########################## PROTOTYPY FUNKCÍ #################################

/**
 * Funkce představuje životní cyklus klienta (autentizuje se serveru a komunikuje s ním pomocí zabezpečeného tunelu, po zaslání všech zpráv serveru končí)
 */
void startClient();

/**
 * Tato poněkud delší funkce (předávání pole typu mpz_t při volání funkce se ukázalo jako problém, proto vnitřek této funkce není dekomponován)
 * představuje implementaci feigeFiatShamir identifikačního schématu, při kterém se klient autentizuje serveru
 * @param secretKeyReduced tajný 256b klíč, pro symetrickou kryptografickou šifru AES vypočítaný a redukovaný z DH
 * @param iv inicilaizační 128b vektor použitý při CBC režimu AES, získaný redukcí z DH
 * @param write_in_pipe_from_client_to_server deskriptor otevřené pojmenované roury pro zápis (směr klient -> server)
 * @param read_from_pipe_from_server_to_client deskriptor otevřené pojmenované roury pro čtení (směr server -> klient)
 * @return 0, pokud autentizace klienta proběhla úspěšně, jinak -1 při jakékoliv chybě
 */
int feigeFiatShamirIdentification(unsigned char* secretKeyReduced, unsigned char* iv, int write_in_pipe_from_client_to_server, int read_from_pipe_from_server_to_client);

/**
 * Funkce postupně zasílá zprávy na server v zašifrované podobě a kontroluje dopovědi od serveru, jestli je hash shodný
 * @param secretKeyReduced tajný 256b klíč, pro symetrickou kryptografickou šifru AES vypočítaný a redukovaný z DH
 * @param iv inicilaizační 128b vektor použitý při CBC režimu AES, získaný redukcí z DH
 * @param write_in_pipe_from_client_to_server deskriptor otevřené pojmenované roury pro zápis (směr klient -> server)
 * @param read_from_pipe_from_server_to_client deskriptor otevřené pojmenované roury pro čtení (směr server -> klient)
 * @return 0, pokud se podařilo odeslat všechny zprávy na server, jinak -1 při jakékoliv chybě
 */
int sendMessagesToServer(unsigned char* secretKeyReduced, unsigned char* iv, int write_in_pipe_from_client_to_server, int read_from_pipe_from_server_to_client);

// ######################### DEFINICE FUNKCÍ ###################################

/**
 * Funkce představuje životní cyklus klienta (autentizuje se serveru a komunikuje s ním pomocí zabezpečeného tunelu, po zaslání všech zpráv serveru končí)
 */
void startClient() {
    
    openSSLInit();

    DH *publicPrivateKeyPair; // DH: soukromý a veřejný klíč jako pár
    
    //********* DH ALGORITMUS: GENEROVANI PARU KLICU ***********
    
    // vygenerovani soukromeho a verejenho klice jako paru
    if (generatePublicPrivateKeyPair(&publicPrivateKeyPair) == -1) {
        fprintf(stderr, "Cyhba při generování páru klíčů\n");
        ERR_print_errors_fp(stderr);
        exit(EXIT_FAILURE);
    }
    
    // převod veřejného klíče do hexadecimální textové reprezentace
    char *publicKeyInHex = BN_bn2hex(publicPrivateKeyPair->pub_key);
    if (publicKeyInHex == NULL) {
        fprintf(stderr, "Chyba při převodu veřejného klíče do hexadecimální textové reprezentace:  %s\n");
        ERR_print_errors_fp(stderr);
        exit(EXIT_FAILURE);
    }
    
    int write_in_pipe_from_client_to_server; // deskriptor otevřené pojmenované roury pro zápis (směr klient -> server)
    int read_from_pipe_from_server_to_client; // deskriptor otevřené pojmenované roury pro čtení (směr server -> klient)

    // otevření pojmenované roury pro zápis (směr klient -> server)
    if ((write_in_pipe_from_client_to_server = open(NAMED_PIPE_FROM_CLIENT_TO_SERVER, O_WRONLY)) == -1) {
        fprintf(stderr, "Chyba: Nebylo možné otevřít pro zápis pojmenovanou rouru:  %s\n", NAMED_PIPE_FROM_CLIENT_TO_SERVER);
        perror("");
        exit(EXIT_FAILURE);
    }
    // otevření pojmenované roury pro čtení (směr server -> klient)
    else if ((read_from_pipe_from_server_to_client = open(NAMED_PIPE_FROM_SERVER_TO_CLIENT, O_RDONLY)) == -1) {
        fprintf(stderr, "Chyba: Nebylo možné otevřít pro čtení pojmenovanou rouru:  %s. Je spuštěna instance serveru?\n", NAMED_PIPE_FROM_SERVER_TO_CLIENT);
        perror("");
        close(write_in_pipe_from_client_to_server);
        exit(EXIT_FAILURE);
    }
    
    //********* DH ALGORITMUS: VYMENA VERJENYCH KLICU PROTISTRAN ***********
    
    // poslani verejneho klice serveru přes pojmenovanou rouru
    if (sendPublicKeyToPeer(publicKeyInHex, write_in_pipe_from_client_to_server) == -1) {
        fprintf(stderr, "Chyba při zasílání veřejného klíče přes rouru:  %s. \n", NAMED_PIPE_FROM_CLIENT_TO_SERVER);
        perror("");
        ERR_print_errors_fp(stderr);
        close(write_in_pipe_from_client_to_server);
        close(read_from_pipe_from_server_to_client);
        exit(EXIT_FAILURE);
    }
    
    // ziskani verejneho klice serveru přes pojemnovanou rouru
    BIGNUM *publicKey = NULL;
    if (receivePublicKeyFromPeer(read_from_pipe_from_server_to_client, &publicKey) == -1) {
        fprintf(stderr, "Chyba při čtení veřejného klíče přes rouru:  %s. \n", NAMED_PIPE_FROM_SERVER_TO_CLIENT);
        perror("");
        ERR_print_errors_fp(stderr);
        close(write_in_pipe_from_client_to_server);
        close(read_from_pipe_from_server_to_client);
        exit(EXIT_FAILURE);
    }
    
    //********* DH ALGORITMUS: VYPOCET SDILENEHO TAJEMSTVI ***********
    
    // vypocet sdileneho tajemstvi o 2048bitech
    unsigned char *secretKey;
    int secretSize;
    if ((secretSize = computeSharedSecret(&secretKey, publicKey, publicPrivateKeyPair)) == -1) {
        fprintf(stderr, "Chyba při výpočtu sdíleného tajemství. \n");
        ERR_print_errors_fp(stderr);
        close(write_in_pipe_from_client_to_server);
        close(read_from_pipe_from_server_to_client);
        exit(EXIT_FAILURE);
    }
    
    //printf("The shared secret is:\n");
    //BIO_dump_fp(stdout, (const char*) secretKey, secretSize);
    //fflush(stdout);
            
    // ziskani redukovaneho klice na 256 bitů z puvodniho klice (sdileneho tajemstvi)
    unsigned char *secretKeyReduced;
    if ((secretKeyReduced = reduce2048BitKeyTo256bits(secretKey)) == NULL) {
        fprintf(stderr, "Chyba při výpočtu redukovaného sdíleného tajného klíče. \n");
        close(write_in_pipe_from_client_to_server);
        close(read_from_pipe_from_server_to_client);
        exit(EXIT_FAILURE);
    }
            
    // ziskani IV (inicializacniho vektoru) o 128 bitech z puvodniho neredukovaneno sdileneho klice
    unsigned char *iv;
    if ((iv = get128BitIVFrom2048BitKey(secretKey)) == NULL) {
        fprintf(stderr, "Chyba při výpočtu inicializačního vektoru ze sdíleného tajného klíče. \n");
        close(write_in_pipe_from_client_to_server);
        close(read_from_pipe_from_server_to_client);
        exit(EXIT_FAILURE);
    }
    
    printf("\n\nKOMUNIKAČNÍ FÁZE 1: AUTENTIZACE KLIENTA POMOCÍ FEIGE-FIAT-SHAMIR\n\n");
    // provedeme autentizaci klienta vůči serveru pomocí feige-fiat-shamira
    if (feigeFiatShamirIdentification(secretKeyReduced, iv, write_in_pipe_from_client_to_server, read_from_pipe_from_server_to_client) == -1) {
        close(write_in_pipe_from_client_to_server);
        close(read_from_pipe_from_server_to_client);
        exit(EXIT_FAILURE);
    }

    printf("\n\nKOMUNIKAČNÍ FÁZE 2: ZASÍLÁNÍ ZPRÁV SERVERU\n\n");
    // postupně zašleme všechny zprávy na server v zašifrované podobě a kontrolujeme dopovědi od serveru, jestli je hash shodný
    if (sendMessagesToServer(secretKeyReduced, iv, write_in_pipe_from_client_to_server, read_from_pipe_from_server_to_client) == -1) {
        close(write_in_pipe_from_client_to_server);
        close(read_from_pipe_from_server_to_client);
        exit(EXIT_FAILURE);
    }
    
    printf("Všechny zprávy byly zaslány serveru a ten byl ukončen zprávou END, nyní se ukončí klient.\n\n");

    
    close(write_in_pipe_from_client_to_server);
    close(read_from_pipe_from_server_to_client);
    
    openSSLcleanUp();
}

/**
 * Tato poněkud delší funkce (předávání pole typu mpz_t při volání funkce se ukázalo jako problém, proto vnitřek této funkce není dekomponován)
 * představuje implementaci feigeFiatShamir identifikačního schématu, při kterém se klient autentizuje serveru
 * Implementace vychází z knihy Handbook of Applied Cryptography
 * @param secretKeyReduced tajný 256b klíč, pro symetrickou kryptografickou šifru AES vypočítaný a redukovaný z DH
 * @param iv inicilaizační 128b vektor použitý při CBC režimu AES, získaný redukcí z DH
 * @param write_in_pipe_from_client_to_server deskriptor otevřené pojmenované roury pro zápis (směr klient -> server)
 * @param read_from_pipe_from_server_to_client deskriptor otevřené pojmenované roury pro čtení (směr server -> klient)
 * @return 0, pokud autentizace klienta proběhla úspěšně, jinak -1 při jakékoliv chybě
 */
int feigeFiatShamirIdentification(unsigned char* secretKeyReduced, unsigned char* iv, int write_in_pipe_from_client_to_server, int read_from_pipe_from_server_to_client) {
    // deklarace proměnných související s komunikací mezi klientem a serverem
    unsigned char receivedPlainMessage[1000]; // dešiforavaná plain text zpráva od serveru, posilame si velka čísla, takže 1000 číslic na reprezentaci by mělo stačit 
    char receivedPlainMessageAsString[1001]; // kvůli vypsání přijaté zprávy bude potřeba pomocné pole, kam zprávu překopírujeme a nakonec dáem ukončovatč O 
    
    unsigned char* receivedCipherMessage; // přijatá zašifrovaná zpráva od serveru
    int receivedCipherMessageLength; // délka přijaté zašifrované zprávy od serveru
    int decryptedMessageLength; // délka rozšifrované zprávy (plaintextu) od serveru
    
    unsigned char *plainMessage; // otevřená zpráva určená k zaslání na server, případně určená pro pomocné výpiy
    unsigned char cipherMessage[1000]; // otevřená zpráva určená k zaslání na server, posilame si velka čísla, takže 1000 číslic na reprezentaci by mělo stačit 
    int cipherMessageLength;  // délka zašifrované zprávy určené k zaslání na server
    
    
    // Handbook of Applied Cryptography, kap.: 10.4.2, krok: 1.
    // veřejný modulus
    const char *n_str = "85607608361883070139487072005518234154391303259258668170145277603626072507259700154004829969474114946831113599352530071726597191105663477559589305326363051448095733041144989668728120597735377089561152540467595768457656273919356340661823782745226015315325327802612307805861506456951670332607726320254080991848078142552936732279938665522177348648676380017215567994445699527398794660984869453217645658350234339734872965211517440358583367321462505077775617008847199375021975475799691131050049268425189591656036446643350938674629581875311006301181987909195136913240466613603845076721915675800881019187412013581285159001047";
    
    // vektor integerů s
    // Handbook of Applied Cryptography, kap.: 10.4.2, krok: 2. (a)
    const char *s_str_vector[feigeFiatShamir_K_VALUE];
    s_str_vector[0] = "9356472229801901313989885994434865746279265863789749883182720129142392990688200238843243907805323605944828757517758944820609620766859471141019642197329994976783957925238261048883175695033374263765440813986889036370034908521778718029267624328765106742524302274564062024787923947239110293073846290408007702991";
    s_str_vector[1] = "93514739273196706759319779640407876945376062702202175764293391655036724712522923628007751202454225572318945372861807807074896936402516436772059938659157872062395695039312790676703037374279845954020403965701572212942760808429509287146760389283867404489242968338762913325561613323001052989278211761226297868366";
    s_str_vector[2] = "106467469682565350262830439302952387209850304284238062494331856786887876959258871288880175768154524887380000883845884507483094456885875664899004108320002700147295965028060996072649454547290254813092514470646281485697756948636469205451422500481772488447271801744807220049466337686310031923902892223109984427841";
    s_str_vector[3] = "125093695479156305489210293382158594388094390802878851849829445796833025860144572008368873606065887218992811321741769346780623005024199327824747301202091240041152594162774958540651457104467705995067645611117096346329001089333097600146526729192469366017394104126880684608640387624874695465212238623053237298797";
    s_str_vector[4] = "135688669495227140646456591180660585553622219643696168887684290141612943565960964505478288510290110644785145944645723077894823155018808613236635673350053292608111664334715092087694722991841046165351285467336712791574122235727048133947898928850684920458523408129215891496053854999717198615528753717007970471534";

    // deklarace vektorů a proměnných
    mpz_t n, r, b, x, y; // proměnné z algoritmu feigeFiatShamir
    mpz_t randNumber, constant2, tmp, tmp2, constantMinus1, constant10, nMinus1; // pomocné porměnné
    gmp_randstate_t randState; // promenna pro generovani nahodnych cisel
    
    // vektory z algoritmu feigeFiatShamir
    mpz_t s_vector[feigeFiatShamir_K_VALUE];
    mpz_t b_vector[feigeFiatShamir_K_VALUE]; // nahodne bity
    mpz_t v_vector[feigeFiatShamir_K_VALUE]; 
    mpz_t e_vector[feigeFiatShamir_K_VALUE]; // challenge vektor
    
    // iniclialiazce vektorů a proměnných
    for (int i = 0; i < feigeFiatShamir_K_VALUE; i++) {
        mpz_init(s_vector[i]);
        mpz_init(b_vector[i]);
        mpz_init(v_vector[i]);
        mpz_init(e_vector[i]);
    }
    mpz_init(n); mpz_init(r); mpz_init(b); mpz_init(x); mpz_init(y); mpz_init(randNumber);mpz_init(constant2); mpz_init(constantMinus1); mpz_init(constant10); mpz_init(tmp); mpz_init(tmp2); mpz_init(nMinus1);
    gmp_randinit_mt(randState);
    
    // nastavený známých hodnot vektorům a proměnným
    for (int i = 0; i < feigeFiatShamir_K_VALUE; i++) {
        mpz_set_str(s_vector[i], s_str_vector[i], 10);
    }
    mpz_set_str(n,n_str, 10);
    mpz_sub_ui (nMinus1, n, 1);
    mpz_set_si(constant2, 2);
    mpz_set_si(constant2, 2);
    mpz_set_si(constant10, 10);
    mpz_set_si(constantMinus1, -1);
    gmp_randseed_ui (randState, time(0));
    
    /* TOTO JE JEDNORÁZOVÁ INICIALIZAČNÍ FÁZE, KDY KLIENT VYPOČÍTÁ SVŮJ VEŘEJNÝ KLÍČ, 
     * KTERÝ ZVEŘEJNÍ
    // Handbook of Applied Cryptography, kap.: 10.4.2, krok: 2. (a)
    // vypočítáme vektor náhodných bitů 
    for (int i = 0; i < feigeFiatShamir_K_VALUE; i++) {
        mpz_urandomm (randNumber, randState, constant2);
        mpz_set(b_vector[i], randNumber);
    }
    
    // Handbook of Applied Cryptography, kap.: 10.4.2, krok: 2. (b)
    // vypočítáme vektor v, který je součástí veřejného klíče spolu s n
    for (int i = 0; i < feigeFiatShamir_K_VALUE; i++) {
        mpz_powm (tmp, constantMinus1, b_vector[i], n); // vypocet (-1)^b_i
        mpz_pow_ui(tmp2, s_vector[i], 2); // vypocet (s_i^2)
        mpz_invert (tmp2, tmp2, n); // vypocet modularni inverze (s_i^2)
        mpz_mul (tmp, tmp2, tmp); // vypocet (-1)^b_i * (s_i^2)^(-1)
        mpz_mod (v_vector[i], tmp, n); // vypocet (-1)^b_i * (s_i^2)^(-1) mod n
    }
    
    // zveřejním veřejný klíč
    for (int i = 0; i < feigeFiatShamir_K_VALUE;i++) {
        plainMessage = (unsigned char*) mpz_get_str (NULL, 10, v_vector[i]); // převedeme číslo v_i na řetězec
        
        printf("Zveřejňuji v%d: %s\n", i, plainMessage);
        
        free(plainMessage);
        
    } 
    // ukončím program chybou (nechci aby dále pokračoval, chci si jen opravdu vypsat veřejný klíč):
    return -1;
    */
    
    // Handbook of Applied Cryptography, kap.: 10.4.2, krok: 4. několikrát zopakovaný
    // provedem několik kol ověření, abychom zvýšili pravděpodobnost jistoty identity
    for (int i = 0; i < feigeFiatShamir_T_ROUNDS; i++) {
        printf("\n%d kolo autentizace:\n\n", i );
        
        // Handbook of Applied Cryptography, kap.: 10.4.2, krok: 4. (a)
        // ****** VYPOCET A ZASLANI X NA SERVER ******
        
        // vypocet r: 1 <= r <= n-1
        mpz_urandomm (randNumber, randState, nMinus1); 
        mpz_add_ui (r, randNumber, 1);
        // plainMessage = (unsigned char*) mpz_get_str (NULL, 10, r); // zašleme;
        //printf("R: %s\n", plainMessage);
        
        // vypocet x = (-1)^b * (r^2) mod n
        mpz_urandomm (randNumber, randState, constant2); 
        mpz_set(b, randNumber); // vypocet nahodneho bitu b
        // plainMessage = (unsigned char*) mpz_get_str (NULL, 10, b); // zašleme;
        //printf("BIt: %s\n", plainMessage);
        
        mpz_powm (tmp, constantMinus1, b, n); // vypocet (-1)^b
        mpz_pow_ui(tmp2, r, 2);   // vypocet (r^2)
        mpz_mul (tmp, tmp2, tmp); // vypocet (-1)^b * (r^2)
        mpz_mod (x, tmp, n);      // vypocet x = (-1)^b * (r^2) mod n
        
        plainMessage = (unsigned char*) mpz_get_str (NULL, 10, x); // převedem číslo x na řezětec
        
        printf("Zasílám na server x: %s\n", plainMessage);
        
        // zašifrujeme zprávu
        if ((cipherMessageLength = encrypt (plainMessage, strlen ((char *)plainMessage), secretKeyReduced, iv, cipherMessage)) == -1) {
            fprintf(stderr, "Chyba:  Nepodařilo se zašifrovat zprávu:  %s. \n", plainMessage);
            ERR_print_errors_fp(stderr);
            return -1;
        }
        
        // zašleme zprávu na server
        if (sendCipherMessageToPeer(cipherMessage, cipherMessageLength, write_in_pipe_from_client_to_server) == -1) {
            fprintf(stderr, "Chyba:  Chyba při zasílání zprávy na server přes rouru:  %s. \n", NAMED_PIPE_FROM_CLIENT_TO_SERVER);
            perror("");
            return -1;
        }
        
        free(plainMessage);
        
       
        // Handbook of Applied Cryptography, kap.: 10.4.2, krok: 4. (b)
        // ****** PŘIJETÍ CHALLNGE VEKTORU E OD SERVERU ******
        
        printf("Získání challenge vektoru od serveru. \n");
        // získáme jednotlivé složky vektoru e od klienta
        for (int i = 0; i < feigeFiatShamir_K_VALUE;i++) {

            // přečteme zprávu od serveru (v zašifrované podobě)
            if ((receivedCipherMessageLength = receiveCipherMessageFromPeer(read_from_pipe_from_server_to_client, &receivedCipherMessage)) == -1) {
                fprintf(stderr, "Chyba při čtení zprávy od klienta přes rouru:  %s. \n", NAMED_PIPE_FROM_SERVER_TO_CLIENT);
                perror("");
                return -1;
            }

            // rozšifrujeme zprávu
            if ((decryptedMessageLength = decrypt(receivedCipherMessage, receivedCipherMessageLength, secretKeyReduced, iv, receivedPlainMessage)) == -1) {
                fprintf(stderr, "Chyba:  Nepodařilo se rozšifrovat zprávu od klienta\n");
                ERR_print_errors_fp(stderr);
                return -1;
            }

            // převedeme přijatou pzrávu na řětezec a vypíšeme přijatou pzrávu
            for (int i = 0; i < decryptedMessageLength;i++) {
                receivedPlainMessageAsString[i] = (char) receivedPlainMessage[i];
                if (i == decryptedMessageLength - 1) {
                    receivedPlainMessageAsString[i + 1] = '\0';
                }
            }
            printf("Přijato od serveru e%d: %s\n", i, receivedPlainMessageAsString);
            mpz_set_str(e_vector[i], receivedPlainMessageAsString, 10); // uložíme složku challenge vectoru přijatou od serveru

            free(receivedCipherMessage);

        }
        
        // Handbook of Applied Cryptography, kap.: 10.4.2, krok: 4. (c)
        // ****** VYPOCET A ZASLANI Y NA SERVER ******
        
        // vypocitame y
        mpz_set_si(tmp, 1); // inicializace akumulacni promenne
        for (int i = 0; i < feigeFiatShamir_K_VALUE;i++) {
            mpz_powm (tmp2, s_vector[i], e_vector[i], n); // vypocet tmp2 = (s_i^(e_i)
            mpz_mul (tmp, tmp, tmp2);                     // vypocet tmp *= tmp2
        }
        mpz_mul (tmp, r, tmp); // r * pi(s_j^(e_j))
        mpz_mod (y, tmp, n);   // vypocet y = r * pi(s_j^(e_j)) mod n
        
        plainMessage = (unsigned char*) mpz_get_str (NULL, 10, y); // převedeme číslo y na řetězec
        
        printf("Zasílám na server y: %s\n", plainMessage);
        
        // zašifrujeme zprávu
        if ((cipherMessageLength = encrypt (plainMessage, strlen ((char *)plainMessage), secretKeyReduced, iv, cipherMessage)) == -1) {
            fprintf(stderr, "Chyba:  Nepodařilo se zašifrovat zprávu:  %s. \n", plainMessage);
            ERR_print_errors_fp(stderr);
            return -1;
        }
        
        // zašleme zprávu na server
        if (sendCipherMessageToPeer(cipherMessage, cipherMessageLength, write_in_pipe_from_client_to_server) == -1) {
            fprintf(stderr, "Chyba:  Chyba při zasílání zprávy na server přes rouru:  %s. \n", NAMED_PIPE_FROM_CLIENT_TO_SERVER);
            perror("");
            return -1;
        }
        
        free(plainMessage);
        
    }
    
    // OVĚŘÍME JESTLI AUTENTIZACE PROBĚHLA ÚSPĚŠNĚ (POKUD BY NEPROBĚHLA NASTALA BY PŘI ČTENÍ Z ROURY CHYBA)
    // Ověříme tedy, jestli: Handbook of Applied Cryptography, kap.: 10.4.2, krok: 4. (d) proběhl na serveru úspěšně
    // přečteme zprávu od klienta (v zašifrované podobě)
    if ((receivedCipherMessageLength = receiveCipherMessageFromPeer(read_from_pipe_from_server_to_client, &receivedCipherMessage)) == -1) {
        fprintf(stderr, "Chyba při čtení zprávy od servera přes rouru:  %s. \n", NAMED_PIPE_FROM_SERVER_TO_CLIENT);
        fprintf(stderr, "Autentizace se pravděpodobně nezdařila\n");
        perror("");
        return -1;
    }

    // rozšifrujeme zprávu
    if ((decryptedMessageLength = decrypt(receivedCipherMessage, receivedCipherMessageLength, secretKeyReduced, iv, receivedPlainMessage)) == -1) {
        fprintf(stderr, "Chyba:  Nepodařilo se rozšifrovat zprávu od serveru\n");
        ERR_print_errors_fp(stderr);
        return -1;
    }

    // převedeme přijatou pzrávu na řětezec a vypíšeme přijatou pzrávu
    for (int i = 0; i < decryptedMessageLength; i++) {
        receivedPlainMessageAsString[i] = (char) receivedPlainMessage[i];
        if (i == decryptedMessageLength - 1) {
            receivedPlainMessageAsString[i + 1] = '\0';
        }
    }
    printf("%s\n", receivedPlainMessageAsString); // mělo by vypsat autehtization success

    free(receivedCipherMessage);
    
    // uvolnění paměti
    for (int i = 0; i < feigeFiatShamir_K_VALUE; i++) {
        mpz_clear(s_vector[i]);
        mpz_clear(b_vector[i]);
        mpz_clear(v_vector[i]);
        mpz_clear(e_vector[i]);
    }

    mpz_clear(n); mpz_clear(r); mpz_clear(b); mpz_clear(x); mpz_clear(y); mpz_clear(randNumber); mpz_clear(constant2);
    
}

/**
 * Funkce postupně zasílá zprávy na server v zašifrované podobě a kontroluje dopovědi od serveru, jestli je hash shodný
 * @param secretKeyReduced tajný 256b klíč, pro symetrickou kryptografickou šifru AES vypočítaný a redukovaný z DH
 * @param iv inicilaizační 128b vektor použitý při CBC režimu AES, získaný redukcí z DH
 * @param write_in_pipe_from_client_to_server deskriptor otevřené pojmenované roury pro zápis (směr klient -> server)
 * @param read_from_pipe_from_server_to_client deskriptor otevřené pojmenované roury pro čtení (směr server -> klient)
 * @return 0, pokud se podařilo odeslat všechny zprávy na server, jinak -1 při jakékoliv chybě
 */
int sendMessagesToServer(unsigned char* secretKeyReduced, unsigned char* iv, int write_in_pipe_from_client_to_server, int read_from_pipe_from_server_to_client) {
// řetezce/zprávy/dotazy k odeslání na server
    char const *messagesToSend[NUM_OF_MESSAGES];
    messagesToSend[0] = "Toto je zprava pro server cislo 1";
    messagesToSend[1] = "Toto je zprava pro server cislo 2";
    messagesToSend[2] = "Toto je zprava pro server cislo 3";
    messagesToSend[3] = "Toto je zprava pro server cislo 4";
    messagesToSend[4] = "Toto je zprava pro server cislo 5";
    messagesToSend[5] = "END";
    
    unsigned char *plainMessage; // otevřená zpráva určená k zaslání na server
    unsigned char cipherMessage[100]; // otevřená zpráva určená k zaslání na server
    unsigned char receivedPlainMessage[100]; // dešiforavaná plain text zpráva od serveru, v praxi to bude hash, který zasílá server 256bitový, stačilo by tedy 32 bajtů, ale z hlediska škálování je lepší nechat větší prostor
    
    unsigned char* receivedCipherMessage; // přijatá zašifrovaná zpráva od serveru
    int receivedCipherMessageLength; // délka přijaté zašifrované zprávy od serveru
    int cipherMessageLength; // délka zašifrované zprávy určené k zaslání na server
    int decryptedMessageLength; // délka rozšifrované zprávy (plaintextu) od serveru
    
    unsigned char* digestOfMessage;    // hash přijaté zprávy
    unsigned int digestOfMessageLength; //délka hashe přijaté zprávy (měla by být vždy 256)
    
    // postupně zašleme všechny zprávy na server v zašifrované podobě a kontrolujeme dopovědi od serveru, jestli je hash shodný
    for (int i = 0; i < NUM_OF_MESSAGES;i++) {
        plainMessage = (unsigned char *) messagesToSend[i]; // aktuální zpáva která se bude odesílat
        
        printf("Zasílám zprávu na server zprávu: %s\n", plainMessage);
                
        // vytvoříme hash zprávy
        if (digestMessageWithSHA256(plainMessage, strlen((char*) plainMessage), &digestOfMessage, &digestOfMessageLength) == -1) {
            fprintf(stderr, "Chyba:  Nepodařilo se vytvořit hash zprávy:  %s. \n", plainMessage);
            ERR_print_errors_fp(stderr);
            return -1;
        }
        
        // zašifrujeme zprávu
        if ((cipherMessageLength = encrypt (plainMessage, strlen ((char *)plainMessage), secretKeyReduced, iv, cipherMessage)) == -1) {
            fprintf(stderr, "Chyba:  Nepodařilo se zašifrovat zprávu:  %s. \n", plainMessage);
            ERR_print_errors_fp(stderr);
            return -1;
        }
        
        // zašleme zprávu na server
        if (sendCipherMessageToPeer(cipherMessage, cipherMessageLength, write_in_pipe_from_client_to_server) == -1) {
            fprintf(stderr, "Chyba:  Chyba při zasílání zprávy na server přes rouru:  %s. \n", NAMED_PIPE_FROM_CLIENT_TO_SERVER);
            perror("");
            return -1;
        }
        
        // přečteme odpověď serveru (hash naší odeslané zprávy)
        if ((receivedCipherMessageLength = receiveCipherMessageFromPeer(read_from_pipe_from_server_to_client, &receivedCipherMessage)) == -1) {
            fprintf(stderr, "Chyba při čtení zprávy se serveru přes rouru:  %s. \n", NAMED_PIPE_FROM_SERVER_TO_CLIENT);
            perror("");
            return -1;
        }
        
        // rozšifrujeme zprávu
        if ((decryptedMessageLength = decrypt(receivedCipherMessage, receivedCipherMessageLength, secretKeyReduced, iv, receivedPlainMessage)) == -1) {
            fprintf(stderr, "Chyba:  Nepodařilo se rozšifrovat zprávu od serveru\n");
            ERR_print_errors_fp(stderr);
            return -1;
        }
        
        // ověříme hash zaslaný serverem, oproti námi vypočítanému hashi 
        if (decryptedMessageLength != digestOfMessageLength) {
            fprintf(stderr, "Chyba: Hash odeslané zprávy a hash zprávy zaslaný serverem se neshoduje\n");
            return -1;
        }
        
        for (int i = 0;i < digestOfMessageLength;i++) {
            if (digestOfMessage[i] != receivedPlainMessage[i]) {
                fprintf(stderr, "Chyba: Hash odeslané zprávy a hash zprávy zaslaný serverem se neshoduje\n");
                return -1;
            }
        }
        
        printf("Server potvrdil integritu odeslané zprávy\n\n");
        
    }
    
}



