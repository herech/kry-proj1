/*
 * Autor: Jan Herec (xherec00)
 * Datum: 16. března 2017
 * Soubor: client_server_common.hpp
 * Komentar: Hlavičkový soubor představující implementaci společných funkcí, které využívají klient i server
 * Kodovani: UTF-8
 */ 

#ifndef CLIENT_SERVER_COMMON_HPP
#define CLIENT_SERVER_COMMON_HPP

#define feigeFiatShamir_K_VALUE 5 // hodnota k u feige-fiat.shamir schématu
#define feigeFiatShamir_T_ROUNDS 4 // počet kol při ověření pomocí feige-fiat.shamir schématu

/**
 * Funkce na úklid v souvislosti OPenSLL knihovnou
 */
void openSSLcleanUp();

/**
 * Funkce na inicializaci v souvislosti OPenSLL knihovnou
 */
void openSSLInit();

/**
 * Funkce počle přes pojmenovanou rouru veřejný klíč z DH protistraně
 * @param publicKeyInHex veřejný klíč, který chcem poslat protistraně
 * @param pipeDescriptor deskriptor otevřené roury pro zápis
 * @return 0, pokud se operace podařila, jinak -1
 */
int sendPublicKeyToPeer(char * publicKeyInHex, int pipeDescriptor);

/**
 * Funkce zapíše do pojemnované roury počet bajtů, které představují velikost následně zaslané zprávy, aby druhá protistrana věděla kdy má se čtením skončit, tak aby přečetla celou zprávu
 * @param numberOfBytes počet bajtů následně zaslné zprávy
 * @param pipeDescriptor dektiptor otevřené roury pro zápis
 * @return 0, pokud se operace podařila, jinak -1
 */
int sendNumBytesToWrite(int numberOfBytes, int pipeDescriptor);

/**
 * Funkce přečte z pojemnované roury počet bajtů, které představují velikost následně zaslané zprávy, víme tak, kolik bajtů přečíst, abychom přečetli celou zprávu
 * @param pipeDescriptor  deskriptor otevřené roury pro čtení
 * @return počet bajtů, které je třeba načíst, abchom načetli celou zaslanou zprávu nebo v případě chyb -1
 */
int getNumBytesToRead(int pipeDescriptor);

/**
 * Funkce z pojmenované roury přečte veřejný klíč zaslaný protistranou
 * @param pipeDescriptor deskriptor otevřené roury pro čtení
 * @param publicKey výstupní parametr, přes který funkce předá načtený veřejný klíč z roury
 * @return 0, pokud se operace podařila, jinak -1
 */
int receivePublicKeyFromPeer(int pipeDescriptor, BIGNUM **publicKey);

/**
 * Funkce která vypočítá sdílené tajemství u DH algoritmu
 * @param secret výstupní parametr představující vypočítané sdílené tajemství
 * @param publicKey veřejný klíč protistrany
 * @param publicPrivateKeyPair pár soukromého a veřejného klíče naší strany
 * @return délka (v bajtech) sdíleného tajemství nebo -1 v případě chyby
 */
int computeSharedSecret(unsigned char **secret, BIGNUM *publicKey, DH *publicPrivateKeyPair);

/**
 * Funkce vygeneruje v rámci DH algoritmu pár soukromého a veřejného klíče, klíče mají velikost 2048b
 * @param publicPrivateKeyPair výstupní parametr představující  strukturu obsahující vygenerovaný pár soukromého a veřejného klíče 
 * @return 0, pokud se operace podařila, jinak -1
 */
int generatePublicPrivateKeyPair(DH **publicPrivateKeyPair);

/**
 * Funkce šifruje otevřený text pomocí AES CBC a 256bit klíče. Je převzata ze stránek https://wiki.openssl.org/index.php/EVP_Symmetric_Encryption_and_Decryption
 * jakožto best practise a pro potřeby projektu mírně upravena
 * @param plaintext otevřený text, který bude šifrován
 * @param plaintext_len délka otevřeho textu (v bajtech)
 * @param key 256bit tajný klíč, kterým se bude šifrovat
 * @param iv 128bit Inicializační vektor
 * @param ciphertext výstupní parametr představující zašifrovaný text
 * @return vrací délku zašifrovaného textu
 */
int encrypt(unsigned char *plaintext, int plaintext_len, unsigned char *key, unsigned char *iv, unsigned char *ciphertext);

/**
 * Funkce dešifruje šifrovaný text. Je převzata ze stránek https://wiki.openssl.org/index.php/EVP_Symmetric_Encryption_and_Decryption
 * jakožto best practise a pro potřeby projektu mírně upravena
 * @param ciphertext šifrovaný text, který bude dešifrován
 * @param ciphertext_len délka šifrovaného textu (v bajtech)
 * @param key 256bit tajný klíč, kterým se bude dešifrovat
 * @param iv 128bit Inicializační vektor
 * @param plaintext  výstupní parametr představující defišofrovaný text
 * @return vrací délku dešifrovaného textu
 */
int decrypt(unsigned char *ciphertext, int ciphertext_len, unsigned char *key, unsigned char *iv, unsigned char *plaintext);

/**
 * Funkce zašle šifrovanou zprávu protistraně přes pojemnovanou rouru 
 * @param cipherMessage šifrovaná zpráva
 * @param cipherMessageLength délka šifrované zprávy
 * @param pipeDescriptor deskriptor pojemnované roury otevřené pro zápis
 * @return 0, pokud vše proběhlo v pořádku, jinak -1
 */
int sendCipherMessageToPeer(unsigned char * cipherMessage, int cipherMessageLength, int pipeDescriptor);

/**
 * Funkce získá přes pojmenovanou rouru šifrovanou zprávu od protistrany
 * @param pipeDescriptor deskriptor pojemnované roury otevřené pro čtení
 * @param cipherMessage výstupní parametr představující získanou šifrovanou zprávu
 * @return -1 pokud chyba jinka počet načtených bytů (velikost zašifroané zprávy)
 */
int receiveCipherMessageFromPeer(int pipeDescriptor, unsigned char **cipherMessage);

/**
 * Funkce vytvoří hash zprávy pomocí SHA-256. Je převzata ze stránek https://wiki.openssl.org/index.php/EVP_Message_Digests
 * jakožto best practise a pro potřeby projektu mírně upravena
 * @param message zpráva, ze které bude vytvářen hash
 * @param message_len délka zprávy, ze které bude vytvářen hash
 * @param digest výstupní parametr představuje hash zprávy
 * @param digest_len délka hashe zprávy
 * @return 0, pokud nastala chyba tak -1
 */
int digestMessageWithSHA256(const unsigned char *messageToDigest, size_t messageToDigestLenght, unsigned char **digestOfMessage, unsigned int *digestOfMessageLength);

/**
 * Získáme redukovaný 256bit klíč z půsovního 2048bit klíče a to tak, že vezmeme každý sudý bajt od začátku tohoto klíče, dokud nedostaneme počet bajtů 32 = počet bitů 256
 * @param key sdílené tajmeství získané z DH
 * @return tajný 256bit klíč pro symetrické  šifrování 
 */
unsigned char * reduce2048BitKeyTo256bits(unsigned char *key);

/**
 * Získáme inicializační vektor z půsovního 2048bit klíče a to tak, že vezmeme reverzovaně jeho posledních 128 bitů
 * @param key sdílené tajmeství získané z DH
 * @return 128 bit inicializační vektor pro symetrické šifrování
 */
unsigned char * get128BitIVFrom2048BitKey(unsigned char *key);

#endif /* CLIENT_SERVER_COMMON_HPP */

